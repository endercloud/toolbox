package org.apache.cayenne.di.spi;

import org.apache.cayenne.di.*;

/**
 * A default implementation of {@link AdhocObjectFactory} that creates objects
 * using default no-arg constructor and injects dependencies into annotated
 * fields. Note that constructor injection is not supported by this factory.
 *
 * Modified for bukkit
 *
 * @since 3.1
 */
public class DefaultAdhocObjectFactory implements AdhocObjectFactory {

    protected Injector injector;
    protected ClassLoaderManager classLoaderManager;

    /**
     * @since 4.0
     */
    public DefaultAdhocObjectFactory(@Inject Injector injector, @Inject ClassLoaderManager classLoaderManager) {
        this.injector = injector;
        this.classLoaderManager = classLoaderManager;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T newInstance(Class<? super T> superType, String className) {

        if (superType == null) {
            throw new NullPointerException("Null superType");
        }

        if (className == null) {
            throw new NullPointerException("Null className");
        }

        Class<T> type = (Class<T>) getJavaClass(className);

        if (!superType.isAssignableFrom(type)) {
            throw new DIRuntimeException("Class %s is not assignable to %s", className, superType.getName());
        }

        T instance;
        try {
            Provider<T> provider0 = new ConstructorInjectingProvider<T>(type, (DefaultInjector) injector);
            Provider<T> provider1 = new FieldInjectingProvider<T>(provider0, (DefaultInjector) injector);
            instance = provider1.get();
        } catch (Exception e) {
            throw new DIRuntimeException("Error creating instance of class %s of type %s", e, className,
                    superType.getName());
        }

        return instance;
    }

    @Override
    public Class<?> getJavaClass(String className) {
        if (className == null) {
            throw new NullPointerException("Null class name");
        }

        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            if (!className.endsWith("[]")) {
                if ("byte".equals(className)) {
                    return Byte.TYPE;
                } else if ("int".equals(className)) {
                    return Integer.TYPE;
                } else if ("short".equals(className)) {
                    return Short.TYPE;
                } else if ("char".equals(className)) {
                    return Character.TYPE;
                } else if ("double".equals(className)) {
                    return Double.TYPE;
                } else if ("long".equals(className)) {
                    return Long.TYPE;
                } else if ("float".equals(className)) {
                    return Float.TYPE;
                } else if ("boolean".equals(className)) {
                    return Boolean.TYPE;
                } else if ("void".equals(className)) {
                    return Void.TYPE;
                }
                // try inner class often specified with "." instead of $
                else {
                    int dot = className.lastIndexOf('.');
                    if (dot > 0 && dot + 1 < className.length()) {
                        className = className.substring(0, dot) + "$" + className.substring(dot + 1);
                        try {
                            return Class.forName(className);
                        } catch (ClassNotFoundException nestedE) {
                            // ignore, throw the original exception...
                        }
                    }
                }

                throw new DIRuntimeException("Invalid class: %s", e, className);
            }

            if (className.length() < 3) {
                throw new IllegalArgumentException("Invalid class name: " + className);
            }

            // TODO: support for multi-dim arrays
            className = className.substring(0, className.length() - 2);

            if ("byte".equals(className)) {
                return byte[].class;
            } else if ("int".equals(className)) {
                return int[].class;
            } else if ("long".equals(className)) {
                return long[].class;
            } else if ("short".equals(className)) {
                return short[].class;
            } else if ("char".equals(className)) {
                return char[].class;
            } else if ("double".equals(className)) {
                return double[].class;
            } else if ("float".equals(className)) {
                return float[].class;
            } else if ("boolean".equals(className)) {
                return boolean[].class;
            }

            try {
                return Class.forName("[L" + className + ";");
            } catch (ClassNotFoundException e1) {
                throw new DIRuntimeException("Invalid class: %s", e1, className);
            }
        }
    }
}
