package com.rhonim.toolbox;

import com.google.inject.Inject;
import com.rhonim.toolbox.commands.ToolboxCommand;
import com.rhonim.toolbox.injection.AppInjector;
import com.rhonim.toolbox.injection.GuiceServiceLoader;
import com.rhonim.toolbox.injection.InjectionRoot;
import com.rhonim.toolbox.injection.Service;
import com.rhonim.toolbox.injection.ToolboxModule;
import com.rhonim.toolbox.toolbox.ToolboxServices;
import net.kyori.adventure.platform.bukkit.BukkitAudiences;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Toolbox extends JavaPlugin implements InjectionRoot {

    private static Toolbox instance;
    private static BukkitAudiences audiences; //Until I find somewhere better to put it

    @Inject
    private ToolboxServices toolboxServices;

    @Override
    public void onLoad() {
        instance = this;

        AppInjector.registerInjectionRoot(this);
        AppInjector.registerRootModule(new ToolboxModule(this));

        if (!this.getDataFolder().exists()) {
            this.getDataFolder().mkdirs();
        }
    }

    @Override
    public void onEnable() {
        audiences = BukkitAudiences.create(this);
        AppInjector.boot();
        this.toolboxServices.boot();

        GuiceServiceLoader.load(ToolboxCommand.class, getClassLoader()).forEach(command -> Bukkit.getCommandMap().register("toolbox", command));
        GuiceServiceLoader.load(Service.class, getClassLoader()).forEach(Service::init);
        GuiceServiceLoader.load(Listener.class, getClassLoader()).forEach(listener -> this.getServer().getPluginManager().registerEvents(listener, this));
    }

    @Override
    public void onDisable() {

    }

    public static Toolbox getInstance() {
        return instance;
    }

    public static BukkitAudiences getAudiences() {
        return audiences;
    }
}
