package com.rhonim.toolbox.util;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.gsonfire.GsonFireBuilder;
import net.dongliu.gson.GsonJava8TypeAdapterFactory;

import java.io.IOException;
import java.util.UUID;

public final class Gson {
    public static final com.google.gson.Gson GSON = new GsonFireBuilder()
            .createGsonBuilder()
            .setPrettyPrinting()
            .registerTypeAdapter(UUID.class, new TypeAdapter<UUID>() {
                @Override
                public void write(JsonWriter out, UUID value) throws IOException {
                    out.value(value.toString());
                }

                @Override
                public UUID read(JsonReader in) throws IOException {
                    return UUID.fromString(in.nextString());
                }
            }.nullSafe())
            .registerTypeAdapterFactory(new GsonJava8TypeAdapterFactory())
            .create();
}

