package com.rhonim.toolbox.util;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.projectiles.ProjectileSource;

public class EntityUtil {
    public static Player getPlayerFromEntity(Entity entity) {
        if (entity instanceof Player) {
            return (Player) entity;
        } else if (entity instanceof Projectile) {
            ProjectileSource source = ((Projectile) entity).getShooter();
            if ((source instanceof Player)) {
                return (Player) source;
            }
        } else if (entity instanceof TNTPrimed) {
            Entity source = ((TNTPrimed) entity).getSource();

            if ((source instanceof Player)) {
                return (Player) source;
            }
        }

        return null;
    }

}
