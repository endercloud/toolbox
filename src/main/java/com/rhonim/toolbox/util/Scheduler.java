package com.rhonim.toolbox.util;

import com.rhonim.toolbox.Toolbox;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;


public final class Scheduler {

    private static final Map<Integer, Supplier<Boolean>> QUEUED_TASKS = new LinkedHashMap<>();

    static {
        Scheduler.repeatAsync(() -> QUEUED_TASKS.entrySet().removeIf(entry -> {

            if (entry.getValue().get()) {
                Bukkit.getScheduler().cancelTask(entry.getKey());
                return true;
            }

            return false;
        }), 0L, 1L);
    }

    /**
     * Run a task synchronously on the main thread using
     * the {@link BukkitScheduler}.
     *
     * @param run The {@link Runnable} to execute.
     * @return The {@link BukkitTask} that is returned after registering the task.
     */
    public static BukkitTask sync(final Runnable run) {
        return Bukkit.getScheduler().runTask(Toolbox.getInstance(), run);
    }

    /**
     * Run a task asynchronously on a separate thread using
     * the {@link BukkitScheduler}.
     *
     * @param run The {@link Runnable} to execute.
     * @return The {@link BukkitTask} that is returned after registering the task.
     */
    public static BukkitTask async(final Runnable run) {
        return Bukkit.getScheduler().runTaskAsynchronously(Toolbox.getInstance(), run);
    }

    /**
     * Run a task synchronously after a specified amount of
     * ticks using the {@link BukkitScheduler}.
     *
     * @param run   The {@link Runnable} task to execute.
     * @param delay The ticks (1 tick = 50 milliseconds, 20 ticks = 1 second) after which to run the task.
     * @return The {@link BukkitTask} that is returned after registering the task.
     */
    public static BukkitTask later(final Runnable run, final long delay) {
        return Bukkit.getScheduler().runTaskLater(Toolbox.getInstance(), run, delay);
    }

    /**
     * Run a task asynchronously after a specified amount of
     * ticks using the {@link BukkitScheduler}.
     *
     * @param run   The {@link Runnable} task to execute.
     * @param delay The ticks (1 tick = 50 milliseconds, 20 ticks = 1 second) after which to run the task.
     * @return The {@link BukkitTask} that is returned after registering the task.
     */
    public static BukkitTask laterAsync(final Runnable run, final long delay) {
        return Bukkit.getScheduler().runTaskLaterAsynchronously(Toolbox.getInstance(), run, delay);
    }

    /**
     * Run a task synchronously repeatedly after a specified amount
     * of ticks and repeated every period ticks until cancelled.
     *
     * @param run    The {@link Runnable} task to execute every period.
     * @param delay  The delay in ticks before the first run of the task.
     * @param period The period in ticks to wait until running again after each run.
     * @return The {@link BukkitTask} that is returned after registering the task.
     */
    public static BukkitTask repeat(final Runnable run, final long delay, final long period) {
        return Bukkit.getScheduler().runTaskTimer(Toolbox.getInstance(), run, delay, period);
    }

    /**
     * Run a task asynchronously repeatedly after a specified amount
     * of ticks and repeated every period ticks until canceled.
     *
     * @param run    The {@link Runnable} task to execute every period.
     * @param delay  The delay in ticks before the first run of the task.
     * @param period The period in ticks to wait until running again after each run.
     * @return The {@link BukkitTask} that is returned after registering the task.
     */
    public static BukkitTask repeatAsync(final Runnable run, final long delay, final long period) {
        return Bukkit.getScheduler().runTaskTimerAsynchronously(Toolbox.getInstance(), run, delay, period);
    }

    /**
     * Run a task synchronously repeatedly until the condition is met
     * at which point it will be cancelled.
     * <p>
     * The task will always be executed at least once and will end once
     * the condition is met. The condition is always tested after each
     * run. Therefore, the task will always run at least once before ending
     * the task. For example, <pre>
     *     int k = 10;
     *     () -> k < 10
     * </pre>
     * In this case, the condition is already met, however, the task will
     * still execute once similar to a do-while loop.
     * <br>
     * This can easily be solved by checking the condition before scheduling
     * the task.
     *
     * @param run    The {@link Runnable} task to execute every period.
     * @param delay  The delay in ticks before the first run of the task.
     * @param period The period in ticks to wait until running again after each run.
     * @param until  The {@link Supplier} to test when to cancel. When this is <tt>true</tt> the task will be cancelled.
     */
    public static void repeatUntil(final Runnable run, final long delay, final long period, final Supplier<Boolean> until) {
        final BukkitTask task = Scheduler.repeat(run, delay, period);
        QUEUED_TASKS.put(task.getTaskId(), until);
    }

    /**
     * Run a task asynchronously repeatedly until the condition is met
     * at which point it will be cancelled.
     * <p>
     * The task will always be executed at least once and will end once
     * the condition is met. The condition is always tested after each
     * run. Therefore, the task will always run at least once before ending
     * the task. For example, <pre>
     *     int k = 10;
     *     () -> k < 10
     * </pre>
     * In this case, the condition is already met, however, the task will
     * still execute once similar to a do-while loop.
     * <br>
     * This can easily be solved by checking the condition before scheduling
     * the task.
     *
     * @param run    The {@link Runnable} task to execute every period.
     * @param delay  The delay in ticks before the first run of the task.
     * @param period The period in ticks to wait until running again after each run.
     * @param until  The {@link Supplier} to test when to cancel. When this returns <tt>true</tt> the task will be cancelled.
     */
    public static void repeatAsyncUntil(final Runnable run, final long delay, final long period, final Supplier<Boolean> until) {
        final BukkitTask task = Scheduler.repeatAsync(run, delay, period);
        QUEUED_TASKS.put(task.getTaskId(), until);
    }
}