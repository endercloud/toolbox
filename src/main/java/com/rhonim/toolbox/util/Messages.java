package com.rhonim.toolbox.util;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public final class Messages {

    public static final TextComponent NO_PERMISSION = Component.text("You don't have permission to do this!",NamedTextColor.RED);
    public static final TextComponent NOT_WHITELISTED = Component.text("You are not whitelisted on this server!", NamedTextColor.RED);

    public static final Function<Duration, String> FORMAT_DURATION = duration -> {
        StringBuilder timeString = new StringBuilder();
        if (duration.toDays() > 0) {
            timeString.append(duration.toDays()).append(" days");
        }

        long hours = duration.toHours() - TimeUnit.DAYS.toHours(duration.toDays());
        if (hours > 0) {
            if (timeString.length() > 0) timeString.append(", ");
            if (hours < 10) timeString.append("0");
            timeString.append(hours).append(" hours");
        }

        long minutes = duration.toMinutes() - TimeUnit.HOURS.toMinutes(duration.toHours());
        if (minutes > 0) {
            if (timeString.length() > 0) timeString.append(", ");
            if (minutes < 10) timeString.append("0");
            timeString.append(minutes).append(" minutes");
        }

        long seconds = duration.getSeconds() - TimeUnit.MINUTES.toSeconds(duration.toMinutes());
        if (seconds > 0) {
            if (timeString.length() > 0) timeString.append(", ");
            if (seconds < 10) timeString.append("0");
            timeString.append(seconds).append(" seconds");
        }

        return timeString.toString();
    };

    private Messages() {
    }

}
