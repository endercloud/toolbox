package com.rhonim.toolbox.util;

import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import com.rhonim.toolbox.Toolbox;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.bukkit.potion.PotionEffectType.*;

public class ItemUtil {

    public static final PotionEffectType[] POSITIVE_POTIONS = {
            SPEED, FAST_DIGGING, INCREASE_DAMAGE, HEAL, JUMP,
            REGENERATION, DAMAGE_RESISTANCE, FIRE_RESISTANCE, WATER_BREATHING,
            INVISIBILITY, NIGHT_VISION, HEALTH_BOOST, ABSORPTION, SATURATION
    };

    /*private static final Field stackHandle;

    static {
        stackHandle = ReflectionUtil.getField(CraftItemStack.class, "handle");
    }*/

    public static String getEnchantExplanation(Enchantment enchantment) {
        if (enchantment == null || enchantment.getName() == null) return "";
        switch (enchantment.getName().toLowerCase()) {
            case "protection_environmental":
                return "Reduces all damage at the cost of armor durability.";
            case "protection_fire":
                return "Reduces fire and lava damage at the cost of armor durability.";
            case "protection_fall":
                return "Reduces fall damage at the cost of armor durability.";
            case "protection_explosions":
                return "Reduces explosive damage at the cost of armor durability.";
            case "protection_projectile":
                return "Reduces projectile damage at the cost of armor durability.";
            case "oxygen":
                return "Increases the time you can spend underwater.";
            case "water_worker":
                return "Increases mining speed while underwater.";
            case "thorns":
                return "Some melee damage is reflected back at your attacker at the cost of armor durability.";
            case "depth_strider":
                return "Increases movement speed while swimming.";
            case "damage_all":
                return "Increases weapon damage.";
            case "damage_undead":
                return "Increases weapon damage to undead opponents.";
            case "damage_arthropods":
                return "Increases weapon damage to spider opponents.";
            case "knockback":
                return "Increases the distance that enemies are pushed back when hit.";
            case "fire_aspect":
                return "Ignites attacked enemies for a short time.";
            case "loot_bonus_mobs":
                return "Increases the possible loot dropped from monsters and animals.";
            case "dig_speed":
                return "Increases mining speed.";
            case "durability":
                return "Improves overall item durability.";
            case "loot_bonus_blocks":
                return "Increases the amount of resources acquired when mining.";
            case "arrow_damage":
                return "Increases the damage dealt by your arrows.";
            case "arrow_knockback":
                return "Increases the distance that enemies are thrown when hit by your arrows.";
            case "arrow_fire":
                return "Ignites your arrows, lighting those they hit on fire.";
            case "arrow_infinite":
                return "Firing your bow does not consume arrows.";
        }
        return "???";
    }

    /**
     * Determines if an item is invalid or duplicated
     *
     * @param itemStack is the item to check
     * @return true if the item should be removed from player inventories
     */
    public static boolean isInvalidItem(ItemStack itemStack) {
        if (itemStack == null)
            return false;

        return itemStack.getAmount() <= 0
                || itemStack.getType() == Material.BEDROCK;
    }

    /**
     * Determines if an item has a custom name
     *
     * @param item is the itemStack to check
     * @return true if the item has a custom item name
     */
    public static boolean hasName(ItemStack item) {
        return hasMetaData(item) && item.getItemMeta().getDisplayName() != null;
    }

    /**
     * Determines if there's any item meta present
     *
     * @param item the item to check
     * @return true if there's ItemMeta defined
     */
    public static boolean hasMetaData(ItemStack item) {
        return item != null && item.hasItemMeta();
    }

    /**
     * Determines if an item has lore
     *
     * @param item the item to check
     * @return true if there's lore
     */
    public static boolean hasLore(ItemStack item) {
        return hasMetaData(item) && item.getItemMeta().getLore() != null;
    }

    public static List<String> getLore(ItemStack item) {
        if (!hasMetaData(item) || item.getItemMeta().getLore() == null) {
            return new ArrayList<>();
        }
        return item.getItemMeta().getLore();
    }

    public static ItemStack setLore(ItemStack item, List<String> lore) {
        ItemMeta meta = item.getItemMeta();
        meta.setLore(lore);
        item.setItemMeta(meta);

        return item;
    }

    /**
     * Determines if an item has any specific data
     *
     * @param item the item to check
     * @return true if they item's durability is not 0
     */
    public static boolean hasData(ItemStack item) {
        return item.getData() != null;
    }

    /**
     * Determines if an item is enchanted
     *
     * @param item the item to check
     * @return if an item has at least 1 enchantment
     */
    public static boolean hasEnchants(ItemStack item) {
        return item.getEnchantments() != null && !item.getEnchantments().isEmpty();
    }

    /**
     * Determines if a skull item has a head owner defined
     *
     * @param item the item to check
     * @return true if a head owner is defined
     */
    public static boolean hasHeadOwner(ItemStack item) {
        return hasMetaData(item) && item.getItemMeta() instanceof SkullMeta && ((SkullMeta) item.getItemMeta()).getOwner() != null && !((SkullMeta) item.getItemMeta()).getOwner().equals("");
    }

    /**
     * Determines if leather armor has a color defined
     *
     * @param item the item to check
     * @return true if there's a color set
     */
    public static boolean hasLeatherColor(ItemStack item) {
        return hasMetaData(item) && item.getItemMeta() instanceof LeatherArmorMeta;
    }

    public static boolean hasBannerData(ItemStack itemStack) {
        return hasMetaData(itemStack) && itemStack.getItemMeta() instanceof BannerMeta;
    }

    public static boolean hasItemFlags(ItemStack item) {
        return hasMetaData(item) && item.getItemMeta().getItemFlags().size() > 0;
    }

    /**
     * Determines if a potion effect is good
     *
     * @param type the PotionEffectType to check
     * @return true if it's a good potion
     */
    public static boolean isPositive(PotionEffectType type) {
        return Arrays.asList(POSITIVE_POTIONS).contains(type);
    }

    /**
     * Determine if two {@link ItemStack} are similar, without NBT tags
     *
     * @param stack1 the first stack
     * @param stack2 thst stack to compare to
     * @return if its similar
     */
    public static boolean isSimilarNoNBT(ItemStack stack1, ItemStack stack2) {
        return stack1.getType().equals(stack2.getType()) && stack1.getDurability() == stack2.getDurability() && stack1.hasItemMeta() == stack2.hasItemMeta();
    }

    /**
     * Returns a more player-known name for a {@link PotionEffectType}
     *
     * @param type the potion effect to get the name of
     * @return the friendly name
     */
    public static String getPotionName(PotionEffectType type) {
        if (type.getName().equals(PotionEffectType.FAST_DIGGING.getName())) {
            return "Haste";
        } else if (type.getName().equals(PotionEffectType.SLOW_DIGGING.getName())) {
            return "Mining Fatigue";
        } else if (type.getName().equals(PotionEffectType.INCREASE_DAMAGE.getName())) {
            return "Strength";
        } else if (type.getName().equals(PotionEffectType.DAMAGE_RESISTANCE.getName())) {
            return "Resistance";
        } else if (type.getName().equals(PotionEffectType.SPEED.getName())) {
            return "Swiftness";
        }
        return WordUtils.capitalizeFully(type.getName().replace("_", " "));
    }

    /**
     * Returns a more player-known name for an {@link EnchantmentTarget}
     *
     * @param target the enchant target to get the name of
     * @return the friendly name
     */
    public static String getFriendlyEnchantmentTargetName(EnchantmentTarget target) {
        if (target == null) {
            return "Any Item";
        }
        switch (target) {
            case ALL:
                return "Any Item";
            case ARMOR:
                return "Armor";
            case ARMOR_FEET:
                return "Boots";
            case ARMOR_HEAD:
                return "Helmet";
            case ARMOR_LEGS:
                return "Leggings";
            case ARMOR_TORSO:
                return "Chestplate";
            case BOW:
                return "Bow";
            case FISHING_ROD:
                return "Fishing";
            case TOOL:
                return "Tool";
            case WEAPON:
                return "Weapon";
        }
        return "???";
    }

    /**
     * Creates a skull from a URL
     *
     * @param urlToFormat the url
     * @return the newly created skull
     */
    public static ItemStack createSkull(String urlToFormat) {
        String url = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUv" + urlToFormat;
        ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (byte) 3);
        if (urlToFormat.isEmpty()) {
            return head;
        }
        SkullMeta headMeta = (SkullMeta) head.getItemMeta();
        PlayerProfile profile = Bukkit.createProfile("");
        profile.setProperty(new ProfileProperty("textures", url));
        headMeta.setPlayerProfile(profile);
        head.setItemMeta(headMeta);
        return head;
    }

    /**
     * Gets a player-frendly name for the provided {@link Enchantment}
     *
     * @param enchantment the enchantment to get the name of
     * @return the friendly name
     */
    public static String getEnchantmentName(Enchantment enchantment) {

        if (enchantment.getName().equalsIgnoreCase(Enchantment.ARROW_DAMAGE.getName())) {
            return "Power";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.ARROW_FIRE.getName())) {
            return "Flame";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.ARROW_INFINITE.getName())) {
            return "Infinity";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.DAMAGE_ALL.getName())) {
            return "Sharpness";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.DAMAGE_ARTHROPODS.getName())) {
            return "Bane of Arthropods";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.DAMAGE_UNDEAD.getName())) {
            return "Smite";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.ARROW_KNOCKBACK.getName())) {
            return "Punch";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.DEPTH_STRIDER.getName())) {
            return "Depth Strider";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.DIG_SPEED.getName())) {
            return "Efficiency";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.DURABILITY.getName())) {
            return "Unbreaking";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.FIRE_ASPECT.getName())) {
            return "Fire Aspect";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.KNOCKBACK.getName())) {
            return "Knockback";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.LOOT_BONUS_BLOCKS.getName())) {
            return "Fortune";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.LOOT_BONUS_MOBS.getName())) {
            return "Looting";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.LUCK.getName())) {
            return "Luck of the Seas";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.LURE.getName())) {
            return "Lure";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.OXYGEN.getName())) {
            return "Respiration";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.PROTECTION_ENVIRONMENTAL.getName())) {
            return "Protection";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.PROTECTION_EXPLOSIONS.getName())) {
            return "Blast Resistance";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.PROTECTION_FALL.getName())) {
            return "Feather Falling";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.PROTECTION_FIRE.getName())) {
            return "Fire Resistance";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.PROTECTION_PROJECTILE.getName())) {
            return "Projectile Protection";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.SILK_TOUCH.getName())) {
            return "Silk Touch";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.THORNS.getName())) {
            return "Thorns";
        } else if (enchantment.getName().equalsIgnoreCase(Enchantment.WATER_WORKER.getName())) {
            return "Aqua Affinity";
        }

        return WordUtils.capitalizeFully(enchantment.getName().replace("_", " "));
    }

    /**
     * Returns a friendly name for the provided material
     *
     * @param material the material to get the name of
     * @return the friendly name
     */
    public static String getItemName(Material material) {
        return getItemName(new ItemStack(material));
    }

    /**
     * Returns a friendly name for the provided ItemStack. If it {@link #hasName(ItemStack)} then it'll return that
     *
     * @param item the item to get the name of
     * @return the friendly name
     */
    public static String getItemName(ItemStack item) {

        if (hasName(item)) {
            return WordUtils.capitalize(item.getItemMeta().getDisplayName());
        }

        return WordUtils.capitalize(item.getType().name().toLowerCase().replace("_", " "));
    }

    public static byte[] itemToBytes(ItemStack stack) {
        return stack.serializeAsBytes();
    }

    public static ItemStack bytesToStack(byte[] bytes) {
        return ItemStack.deserializeBytes(bytes);
    }

    public static <T> ItemStack applyData(ItemStack stack, String key, T obj) {
        ItemMeta meta = stack.getItemMeta();

        PersistentDataContainer dataContainer = meta.getPersistentDataContainer();
        dataContainer.set(new NamespacedKey(Toolbox.getInstance(), key), getTypeFromObject(obj), obj);

        stack.setItemMeta(meta);
        return stack;
    }

    public static <T> ItemStack applyData(ItemStack stack, NamespacedKey key, T obj) {
        ItemMeta meta = stack.getItemMeta();

        PersistentDataContainer dataContainer = meta.getPersistentDataContainer();
        dataContainer.set(key, getTypeFromObject(obj), obj);

        stack.setItemMeta(meta);
        return stack;
    }

    public static <T> T getData(ItemStack stack, String key, PersistentDataType type) {
        return getData(stack, new NamespacedKey(Toolbox.getInstance(), key), type);
    }

    public static <T> T getData(ItemStack stack, NamespacedKey key, PersistentDataType type) {
        ItemMeta meta = stack.getItemMeta();
        if (!hasMetaData(stack)) {
            return null;
        }

        PersistentDataContainer dataContainer = meta.getPersistentDataContainer();

        return (T) dataContainer.get(key, type);
    }

    public static void removeData(ItemStack stack, String key) {
        removeData(stack, new NamespacedKey(Toolbox.getInstance(), key));
    }

    public static boolean hasData(ItemStack stack, String key, PersistentDataType type) {
        return hasData(stack, new NamespacedKey(Toolbox.getInstance(), key), type);
    }

    public static boolean hasData(ItemStack stack, NamespacedKey key, PersistentDataType type) {
        if (!ItemUtil.hasMetaData(stack)) {
            return false;
        }
        return stack.getItemMeta().getPersistentDataContainer().has(key, type);
    }

    public static void removeData(ItemStack stack, NamespacedKey key) {
        ItemMeta meta = stack.getItemMeta();
        if (meta == null) {
            return;
        }

        meta.getPersistentDataContainer().remove(key);
        stack.setItemMeta(meta);
    }

    private static <T> PersistentDataType getTypeFromObject(T obj) {
        PersistentDataType type;
        if (obj instanceof Byte) {
            type = PersistentDataType.BYTE;
        } else if (obj instanceof Short) {
            type = PersistentDataType.SHORT;
        } else if (obj instanceof Integer) {
            type = PersistentDataType.INTEGER;
        } else if (obj instanceof Long) {
            type = PersistentDataType.LONG;
        } else if (obj instanceof Float) {
            type = PersistentDataType.FLOAT;
        } else if (obj instanceof Double) {
            type = PersistentDataType.DOUBLE;
        } else if (obj instanceof String) {
            type = PersistentDataType.STRING;
        } else if (obj instanceof byte[]) {
            type = PersistentDataType.BYTE_ARRAY;
        } else if (obj instanceof int[]) {
            type = PersistentDataType.INTEGER_ARRAY;
        } else if (obj instanceof long[]) {
            type = PersistentDataType.LONG_ARRAY;
        } else if (obj instanceof PersistentDataContainer) {
            type = PersistentDataType.TAG_CONTAINER;
        } else {
            throw new IllegalStateException("Invalid object provided! " + obj.getClass().getName());
        }

        return type;
    }
}
