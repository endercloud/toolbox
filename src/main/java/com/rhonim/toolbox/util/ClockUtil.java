package com.rhonim.toolbox.util;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClockUtil {
    private static Pattern timePattern = Pattern.compile(
            "(?:([0-9]+)\\s*y[a-z]*[,\\s]*)?"
                    + "(?:([0-9]+)\\s*mo[a-z]*[,\\s]*)?"
                    + "(?:([0-9]+)\\s*w[a-z]*[,\\s]*)?"
                    + "(?:([0-9]+)\\s*d[a-z]*[,\\s]*)?"
                    + "(?:([0-9]+)\\s*h[a-z]*[,\\s]*)?"
                    + "(?:([0-9]+)\\s*m[a-z]*[,\\s]*)?"
                    + "(?:([0-9]+)\\s*(?:s[a-z]*)?)?",
            Pattern.CASE_INSENSITIVE);

    public static String formatTimeSpan(long timeInMilis) {
        StringBuilder time = new StringBuilder();
        double[] timesList = new double[4];
        String[] unitsList = {"second", "minute", "hour", "day"};

        timesList[0] = timeInMilis / 1000.0D;
        timesList[1] = Math.floor(timesList[0] / 60.0D);
        timesList[0] -= timesList[1] * 60.0D;
        timesList[2] = Math.floor(timesList[1] / 60.0D);
        timesList[1] -= timesList[2] * 60.0D;
        timesList[3] = Math.floor(timesList[2] / 24.0D);
        timesList[2] -= timesList[3] * 24.0D;

        for (int j = 3; j > -1; j--) {
            double d = timesList[j];
            if (d >= 1.0D) {
                time.append((int) d).append(" ").append(unitsList[j]).append(d > 1.0D ? "s " : " ");
            }
        }
        return time.toString().trim();
    }

    public static String formatToRelative(long time) {
        LocalDateTime start = LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault());
        LocalDateTime end = LocalDateTime.now();

        StringBuilder output = new StringBuilder();
        Duration duration = start.isBefore(end) ? Duration.between(start, end) : Duration.between(end, start);

        if (duration.toDays() > 0L) {
            output.append(duration.toDays()).append("d ");
            duration = duration.minusDays(duration.toDays());
        }
        if (duration.toHours() > 0L) {
            output.append(duration.toHours()).append("h ");
            duration = duration.minusHours(duration.toHours());
        }
        if (duration.toMinutes() > 0L) {
            output.append(duration.toMinutes()).append("m ");
            duration = duration.minusMinutes(duration.toMinutes());
        }
        if (!duration.isZero()) {
            output.append(duration.getSeconds()).append("s");
        }
        return output.toString();
    }

    public static long parseTime(String time, boolean future) throws Exception {
        int years = 0, months = 0, weeks = 0, days = 0, hours = 0, minutes = 0, seconds = 0;

        Matcher m = timePattern.matcher(time);
        boolean found = false;

        while (m.find()) {
            if (m.group() == null || m.group().isEmpty()) {
                continue;
            }
            for (int i = 0; i < m.groupCount(); i++) {
                if (m.group(i) != null && !m.group(i).isEmpty()) {
                    found = true;
                    break;
                }
            }
            if (found) {
                if (m.group(1) != null && !m.group(1).isEmpty()) {
                    years = Integer.parseInt(m.group(1));
                }
                if (m.group(2) != null && !m.group(2).isEmpty()) {
                    months = Integer.parseInt(m.group(2));
                }
                if (m.group(3) != null && !m.group(3).isEmpty()) {
                    weeks = Integer.parseInt(m.group(3));
                }
                if (m.group(4) != null && !m.group(4).isEmpty()) {
                    days = Integer.parseInt(m.group(4));
                }
                if (m.group(5) != null && !m.group(5).isEmpty()) {
                    hours = Integer.parseInt(m.group(5));
                }
                if (m.group(6) != null && !m.group(6).isEmpty()) {
                    minutes = Integer.parseInt(m.group(6));
                }
                if (m.group(7) != null && !m.group(7).isEmpty()) {
                    seconds = Integer.parseInt(m.group(7));
                }
                break;
            }
        }
        if (!found) {
            throw new Exception("Illegal date format");
        }
        Calendar c = new GregorianCalendar();
        if (years > 0) {
            c.add(Calendar.YEAR, years * (future ? 1 : -1));
        }
        if (months > 0) {
            c.add(Calendar.MONTH, months * (future ? 1 : -1));
        }
        if (weeks > 0) {
            c.add(Calendar.WEEK_OF_YEAR, weeks * (future ? 1 : -1));
        }
        if (days > 0) {
            c.add(Calendar.DAY_OF_MONTH, days * (future ? 1 : -1));
        }
        if (hours > 0) {
            c.add(Calendar.HOUR_OF_DAY, hours * (future ? 1 : -1));
        }
        if (minutes > 0) {
            c.add(Calendar.MINUTE, minutes * (future ? 1 : -1));
        }
        if (seconds > 0) {
            c.add(Calendar.SECOND, seconds * (future ? 1 : -1));
        }
        Calendar max = new GregorianCalendar();
        max.add(Calendar.YEAR, 10);
        if (c.after(max)) {
            return max.getTimeInMillis();
        }
        return c.getTimeInMillis();
    }
}
