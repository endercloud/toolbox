package com.rhonim.toolbox.util;

import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import com.google.common.collect.ImmutableList;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.Dye;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionData;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ItemBuilder {


    private Material material = null;
    private int amount = 1;
    private short durability = -1;
    //private BaseComponent[] customName = null;
    private String customName = null;
    private List<String> lore = null;
    private String headOwner = null;
    private Color leatherColor = null;
    private PotionData basePotion = null;
    private boolean glowing = false;
    private List<ItemFlag> itemFlags = new ArrayList<>();
    private Map<Enchantment, Integer> enchantments = new HashMap<>();
    private Map<Integer, Pattern> bannerPatterns = new HashMap<>();
    private Map<NamespacedKey, CustomData> customData = new HashMap<>();
    private String playerSkin = null;
    private String skinURL = null;

    public ItemBuilder(ItemStack item) {
        this.material = item.getType();
        this.amount = item.getAmount();
        this.durability = item.getDurability();
        ItemMeta meta = item.getItemMeta();
        if (ItemUtil.hasName(item)) {
            //this.customName = meta.getDisplayNameComponent();
            this.customName = meta.getDisplayName();
        }
        if (ItemUtil.hasLore(item)) {
            this.lore = meta.getLore();
        }

        if (ItemUtil.hasEnchants(item)) {
            this.enchantments = new HashMap<>(item.getEnchantments());
        }

        if (ItemUtil.hasHeadOwner(item)) {
            this.headOwner = ((SkullMeta) meta).getOwner();
        }

        if (ItemUtil.hasLeatherColor(item)) {
            LeatherArmorMeta m = (LeatherArmorMeta) meta;
            this.leatherColor = m.getColor();
        }

        if (ItemUtil.hasItemFlags(item)) {
            this.itemFlags.addAll(meta.getItemFlags());
        }

        if (ItemUtil.hasBannerData(item)) {
            BannerMeta bm = (BannerMeta) meta;
            //TODO: Change base color to be based on material
            if (bm.getBaseColor() != null) {
                this.bannerPatterns.put(0, new Pattern(bm.getBaseColor(), PatternType.BASE));
            }
            if (bm.getPatterns() != null && !bm.getPatterns().isEmpty()) {
                int number = 1;
                for (Pattern pattern : bm.getPatterns()) {
                    this.bannerPatterns.put(number++, pattern);
                }
            }
        }

        if (meta instanceof PotionMeta) {
            PotionMeta pm = (PotionMeta) meta;

            this.basePotion = pm.getBasePotionData();
        }

        if (this.material.equals(Material.ENCHANTED_BOOK)) {
            EnchantmentStorageMeta enchantBookMeta = (EnchantmentStorageMeta) meta;
            this.enchantments = enchantBookMeta.getStoredEnchants();
        }

        PersistentDataContainer container = meta.getPersistentDataContainer();
        for (NamespacedKey key : container.getKeys()) {
            PersistentDataType type = this.getDataType(container, key);
            if (type == null) {
                continue;
            }
            this.customData.put(key, new CustomData(type, container.get(key, type)));
        }

    }

    public ItemBuilder(Potion potion) {
        this(potion.toItemStack(1));
    }

    public ItemBuilder(Material material) {
        this.material = material;
    }

    public ItemBuilder(DyeColor dye) {
        Dye dyeItem = new Dye();
        dyeItem.setColor(dye);
        ItemStack is = dyeItem.toItemStack();
        this.material = is.getType();
    }

    public ItemBuilder withBasePostion(PotionData basePotion) {
        this.basePotion = basePotion;
        return this;
    }

    public ItemBuilder asColor(Color color) {
        this.leatherColor = color;
        return this;
    }

    public ItemBuilder withLore(String... lore) {
        this.lore = ImmutableList.copyOf(lore);
        return this;
    }

    public ItemBuilder withLore(Iterable<String> lore) {
        this.lore = ImmutableList.copyOf(lore);
        return this;
    }

    public ItemBuilder addLore(String lore) {
        if (this.lore == null) {
            this.lore = new ArrayList<>();
        }

        this.lore.add(lore);
        return this;
    }

    public ItemBuilder ofType(Material material) {
        this.material = material;
        return this;
    }

    public ItemBuilder setGlow() {
        this.glowing = true;
        return this;
    }

    public ItemBuilder withPlayerSkin(Player player) {
        String skinURL = "";
        for (ProfileProperty profileProperty : player.getPlayerProfile().getProperties()) {
            if (profileProperty.getName().equals("textures")) {
                JsonObject jsonObject = new JsonParser().parse(new String(Base64.getDecoder().decode(profileProperty.getValue()))).getAsJsonObject();
                if (jsonObject.has("textures")) {
                    JsonObject texObj = jsonObject.getAsJsonObject("textures");
                    if (texObj.has("SKIN")) {
                        JsonObject skinObj = texObj.getAsJsonObject("SKIN");
                        if (skinObj.has("url")) {
                            skinURL = skinObj.getAsJsonPrimitive("url").getAsString();
                        }
                    }
                }
            }
        }

        this.skinURL = skinURL;
        return this;
    }


    public ItemBuilder withSkinURL(String skinURL) {
        this.skinURL = skinURL;
        return this;
    }

    public ItemBuilder includeEnchantment(Enchantment enchant, int level) {
        if (enchant == null) {
            return this;
        }
        if (enchantments == null) {
            enchantments = new HashMap<>();
        }
        enchantments.put(enchant, level);
        return this;
    }

    public ItemBuilder includeEnchantments(Map<Enchantment, Integer> enchants) {
        if (enchantments == null) {
            enchantments = enchants;
        } else {
            for (Map.Entry<Enchantment, Integer> set : enchants.entrySet()) {
                enchantments.put(set.getKey(), set.getValue());
            }
        }
        return this;
    }

    public ItemBuilder asHeadOwner(String name) {
        this.headOwner = name;
        return this;
    }


    public ItemBuilder withLore(List<String> lore) {
        if (this.lore != null && !this.lore.isEmpty()) {
            this.lore.addAll(lore);
        } else {
            this.lore = lore;
        }
        return this;
    }

    public ItemBuilder withCustomName(String customName) {
        //this.customName = TextComponent.fromLegacyText(customName);
        this.customName = customName;
        return this;
    }

    /*public ItemBuilder withCustomName(BaseComponent[] baseComponents) {
        this.customName = baseComponents;
        return this;
    }*/

    public ItemBuilder withCustomName(net.kyori.adventure.text.TextComponent customName) {
        //this.customName = BungeeCordComponentSerializer.get().serialize(customName);
        this.customName = LegacyComponentSerializer.legacySection().serialize(customName);
        return this;
    }

    public ItemBuilder withQuantity(int amount) {
        this.amount = amount;
        return this;
    }

    public ItemBuilder flagWith(ItemFlag flag) {
        if (!this.itemFlags.contains(flag)) {
            this.itemFlags.add(flag);
        }
        return this;
    }

    public <T> ItemBuilder withCustomData(NamespacedKey key, PersistentDataType<T, T> type, T obj) {
        this.customData.put(key, new CustomData(type, obj));
        return this;
    }

    public ItemStack build() {
        ItemStack item = new ItemStack(this.material, this.amount);
        if (this.durability != -1) {
            item.setDurability(this.durability);
        }

        ItemMeta meta = item.getItemMeta();

        if (this.customName != null) {
            //meta.setDisplayNameComponent(this.customName);
            meta.setDisplayName(this.customName);
        }
        if (this.lore != null) {
            meta.setLore(this.lore);
        }

        if (this.basePotion != null && (this.material.equals(Material.POTION) || this.material.equals(Material.SPLASH_POTION) || this.material.equals(Material.LINGERING_POTION))) {
            PotionMeta potionMeta = (PotionMeta) meta;
            potionMeta.setBasePotionData(this.basePotion);
            meta = potionMeta;
        }
        if (this.headOwner != null && (this.material.equals(Material.PLAYER_HEAD) || this.material.equals(Material.PLAYER_WALL_HEAD)) && meta instanceof SkullMeta) {
            SkullMeta m = (SkullMeta) meta;
            m.setOwner(headOwner);
            meta = m;
        } else if (this.skinURL != null && !this.skinURL.isEmpty() && (this.material.equals(Material.PLAYER_HEAD) || this.material.equals(Material.PLAYER_WALL_HEAD)) && meta instanceof SkullMeta) {
            SkullMeta m = (SkullMeta) meta;

            PlayerProfile profile = Bukkit.createProfile(UUID.randomUUID(), "");
            byte[] encodedData = Base64.getEncoder().encode(String.format("{textures:{SKIN:{url:\"%s\"}}}", this.skinURL).getBytes());
            profile.setProperty(new ProfileProperty("textures", new String(encodedData)));
            m.setPlayerProfile(profile);

            meta = m;
        }


        if (!this.bannerPatterns.isEmpty() && meta instanceof BannerMeta) {

            BannerMeta m = (BannerMeta) meta;
            int number = 0;
            while (this.bannerPatterns.containsKey(number)) {
                if (number == 0) {
                    m.setBaseColor(bannerPatterns.get(number).getColor());
                } else {
                    m.addPattern(bannerPatterns.get(number));
                }
                number++;
            }

            meta = m;
        }

        if (this.leatherColor != null && meta instanceof LeatherArmorMeta) {
            LeatherArmorMeta m = (LeatherArmorMeta) meta;
            m.setColor(this.leatherColor);
            meta = m;
        }


        if (!this.itemFlags.isEmpty()) {
            for (ItemFlag flag : this.itemFlags) {
                meta.addItemFlags(flag);
            }
        }

        if (this.enchantments != null) {
            if (this.material.equals(Material.ENCHANTED_BOOK)) {
                EnchantmentStorageMeta esm = (EnchantmentStorageMeta) meta;
                for (Map.Entry<Enchantment, Integer> entry : this.enchantments.entrySet()) {
                    esm.addStoredEnchant(entry.getKey(), entry.getValue(), true);
                }
                meta = esm;
            } else {
                for (Map.Entry<Enchantment, Integer> entry : this.enchantments.entrySet()) {
                    meta.addEnchant(entry.getKey(), entry.getValue(), true);
                }
            }
        }


        if (item.getEnchantments().isEmpty() && this.glowing) {
            meta.addEnchant(Enchantment.LURE, 1, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        }

        PersistentDataContainer container = meta.getPersistentDataContainer();
        for (Map.Entry<NamespacedKey, CustomData> entry : this.customData.entrySet()) {
            container.set(entry.getKey(), entry.getValue().getDataType(), entry.getValue().getObj());
        }

        item.setItemMeta(meta);
        return item;
    }

    public static class Skins {
        public static final String MHF_QUESTION = "http://textures.minecraft.net/texture/5163dafac1d91a8c91db576caac784336791a6e18d8f7f62778fc47bf146b6";
        public static final String UNITED_STATES_FLAG = "http://textures.minecraft.net/texture/fcbc32cb24d57fcdc031e851235da2daad3e1914b87043bd012633e6f32c7";
    }


    private PersistentDataType<?, ?> getDataType(PersistentDataContainer container, NamespacedKey key) {
        if (container.has(key, PersistentDataType.BYTE)) {
            return PersistentDataType.BYTE;
        } else if (container.has(key, PersistentDataType.SHORT)) {
            return PersistentDataType.SHORT;
        } else if (container.has(key, PersistentDataType.INTEGER)) {
            return PersistentDataType.INTEGER;
        } else if (container.has(key, PersistentDataType.LONG)) {
            return PersistentDataType.LONG;
        } else if (container.has(key, PersistentDataType.FLOAT)) {
            return PersistentDataType.FLOAT;
        } else if (container.has(key, PersistentDataType.DOUBLE)) {
            return PersistentDataType.DOUBLE;
        } else if (container.has(key, PersistentDataType.STRING)) {
            return PersistentDataType.STRING;
        } else if (container.has(key, PersistentDataType.BYTE_ARRAY)) {
            return PersistentDataType.BYTE_ARRAY;
        } else if (container.has(key, PersistentDataType.INTEGER_ARRAY)) {
            return PersistentDataType.INTEGER_ARRAY;
        } else if (container.has(key, PersistentDataType.LONG_ARRAY)) {
            return PersistentDataType.LONG_ARRAY;
        } else if (container.has(key, PersistentDataType.TAG_CONTAINER_ARRAY)) {
            return PersistentDataType.TAG_CONTAINER_ARRAY;
        } else if (container.has(key, PersistentDataType.TAG_CONTAINER)) {
            return PersistentDataType.TAG_CONTAINER;
        }

        return null;
    }


    private class CustomData<T> {
        private final PersistentDataType<T, T> dataType;
        private final T obj;

        public CustomData(PersistentDataType<T, T> dataType, T obj) {
            this.dataType = dataType;
            this.obj = obj;
        }

        public PersistentDataType<T, T> getDataType() {
            return dataType;
        }

        public T getObj() {
            return obj;
        }
    }
}
