package com.rhonim.toolbox.util;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryUtil {

    public static boolean hasSpaceForItem(Inventory inventory, ItemStack stack) {
        if (stack == null || stack.getType() == Material.AIR) {
            return false;
        }
        int space = 0;

        for (ItemStack itemStack : inventory.getStorageContents()) {
            if (itemStack == null || itemStack.getType() == Material.AIR) {
                space += stack.getType().getMaxStackSize();
            } else if (stack.isSimilar(itemStack)) {
                space += stack.getType().getMaxStackSize() - itemStack.getAmount();
            }

            if (space >= stack.getAmount()) {
                return true;
            }
        }

        return space >= stack.getAmount();
    }
}
