package com.rhonim.toolbox.util;

import com.rhonim.toolbox.Toolbox;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.Map;

public class PlayerUtil {

    public static void giveItemsSafely(Player player, Collection<ItemStack> itemStacks) {
        giveItemsSafely(player, true, itemStacks);
    }

    public static void giveItemsSafely(Player player, ItemStack... itemStack) {
        giveItemsSafely(player, true, itemStack);
    }

    public static void giveItemsSafely(Player player, boolean verbose, Collection<ItemStack> itemStacks) {
        if (itemStacks.isEmpty()) {
            return;
        }

        giveItemsSafely(player, verbose, itemStacks.toArray(new ItemStack[itemStacks.size()]));
    }

    public static void giveItemsSafely(Player player, boolean verbose, ItemStack... itemStacks) {
        Map<Integer, ItemStack> map = player.getInventory().addItem(itemStacks);
        if (!map.isEmpty()) {
            for (Map.Entry<Integer, ItemStack> entry : map.entrySet()) {
                ItemStack itemStack = entry.getValue();
                player.getWorld().dropItem(player.getLocation(), itemStack);
                if (verbose) {
                    Audience audience = Toolbox.getAudiences().player(player);
                    audience.sendMessage(Identity.nil(),
                            Component.text("(", NamedTextColor.GRAY, TextDecoration.ITALIC)
                            .append(LegacyComponentSerializer.legacySection().deserialize(ItemUtil.getItemName(itemStack)))
                            .append(Component.text("dropped on ground due to having a full inventory)", NamedTextColor.GRAY, TextDecoration.ITALIC))
                    );
                    //audience.sendMessage("" + ChatColor.GRAY + ChatColor.ITALIC + "(" + ItemUtil.getItemName(itemStack) + ChatColor.GRAY + ChatColor.ITALIC + " dropped on ground due to having a full inventory.)");
                }
            }
        }
    }

    public static boolean hasSpaceForItem(Player player, ItemStack stack, int amount) {
        if (stack == null || stack.getType() == Material.AIR) {
            return false;
        }
        int space = 0;

        for (ItemStack itemStack : player.getInventory().getStorageContents()) {
            if (itemStack == null || itemStack.getType() == Material.AIR) {
                space += stack.getType().getMaxStackSize();
            } else if (stack.isSimilar(itemStack)) {
                space += stack.getType().getMaxStackSize() - itemStack.getAmount();
            }

            if (space >= amount) {
                return true;
            }
        }

        return space >= amount;
    }

    public static boolean hasSpaceForItem(Player player, ItemStack stack) {
        return InventoryUtil.hasSpaceForItem(player.getInventory(), stack);
    }

    public static int getSpaceForItem(Player player, ItemStack stack) {
        if (stack == null || stack.getType() == Material.AIR) {
            return 0;
        }
        int space = 0;


        for (ItemStack itemStack : player.getInventory().getStorageContents()) {
            if (itemStack == null || itemStack.getType() == Material.AIR) {
                space += stack.getType().getMaxStackSize();
            } else if (stack.isSimilar(itemStack)) {
                space += stack.getType().getMaxStackSize() - itemStack.getAmount();
            }
        }


        return space;
    }

    public static void removeAmount(Player player, ItemStack stack, int amount) {
        for (ItemStack itemStack : player.getInventory().getStorageContents()) {
            if (itemStack == null || itemStack.getType().equals(Material.AIR)) {
                continue;
            }

            if (itemStack.isSimilar(stack)) {
                if (itemStack.getAmount() < amount) {
                    amount -= itemStack.getAmount();
                    itemStack.subtract(itemStack.getAmount()); //Cheaty but SHOULD work
                } else {
                    itemStack.subtract(amount);
                    amount = 0;
                }


                if (amount <= 0) {
                    break;
                }
            }
        }
    }

    public static void removeAll(Player player, ItemStack stack) {
        for (ItemStack itemStack : player.getInventory().getStorageContents()) {
            if (itemStack == null || itemStack.getType().equals(Material.AIR)) {
                continue;
            }

            if (itemStack.isSimilar(stack)) {
                itemStack.subtract(itemStack.getAmount()); //Cheaty but SHOULD work
            }
        }
    }

    public static int getItemCount(Player player, ItemStack stack) {
        if (stack == null || stack.getType() == Material.AIR) {
            return 0;
        }

        int total = 0;
        for (ItemStack itemStack : player.getInventory().getStorageContents()) {
            if (itemStack == null || itemStack.getType().equals(Material.AIR)) {
                continue;
            }

            if (itemStack.isSimilar(stack)) {//We want to make sure the NBT tags match too
                total += itemStack.getAmount();
            }
        }

        return total;
    }

    public static int getEmptyInvenSlots(Player player) {
        int empty = 0;
        for (ItemStack stack : player.getInventory().getStorageContents()) {
            if (stack == null || stack.getType().equals(Material.AIR)) {
                empty++;
            }
        }

        return empty;
    }

//    public static String toJson(Player player) {
//
//        NBTTagCompound nbtTagCompound = new NBTTagCompound();
//        ((CraftPlayer) player).getHandle().b(nbtTagCompound);
//        return StringEscapeUtils.escapeJson(nbtTagCompound.toString());
//    }
//
//    public static void fromJson(Player player, String json) {
//        try {
//            ((CraftPlayer) player).getHandle().a(MojangsonParser.parse(StringEscapeUtils.unescapeJson(json)));
//        } catch (CommandSyntaxException e) {
//            e.printStackTrace();
//        }
//    }
}
