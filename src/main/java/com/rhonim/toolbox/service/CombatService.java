package com.rhonim.toolbox.service;

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.rhonim.toolbox.entities.CombatTag;
import com.rhonim.toolbox.entities.events.PlayerEnterCombatEvent;
import com.rhonim.toolbox.injection.Service;
import com.rhonim.toolbox.repo.CombatRepository;
import com.rhonim.toolbox.util.EntityUtil;
import com.rhonim.toolbox.util.Scheduler;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Singleton
@AutoService(Service.class)
public class CombatService implements Service {
    private final long COMABT_LENGTH = TimeUnit.SECONDS.toMillis(10L);

    private final CombatRepository combatRepository;

    @Inject
    public CombatService(CombatRepository combatRepository) {
        this.combatRepository = combatRepository;
    }

    @Override
    public void init() {
        //Try to keep the map decently small
        Scheduler.repeatAsync(this.combatRepository::cleanMaps, 20L * 10L, 20L * 10L);
    }

    public void handleDamageMonitor(EntityDamageByEntityEvent event) {

        Player attacker = EntityUtil.getPlayerFromEntity(event.getDamager());
        Player victim = EntityUtil.getPlayerFromEntity(event.getEntity());

        if (attacker == null || victim == null) {
            return;
        }

        ImmutableMap<UUID, CombatTag> attackerTags = this.combatRepository.getTags(attacker.getUniqueId());

        //They should share the same tag
        CombatTag tag;
        if ((tag = attackerTags.get(victim.getUniqueId())) != null) {
            tag.setTagExpire(System.currentTimeMillis() + COMABT_LENGTH);
            if (attacker.getUniqueId().equals(tag.getAttacker())) {
                tag.incrementAttackerDamage(event.getDamage());
            } else {
                tag.incrementVictimDamage(event.getDamage());
            }
            return;
        }

        tag = new CombatTag(attacker.getUniqueId(), victim.getUniqueId(), System.currentTimeMillis(), System.currentTimeMillis() + COMABT_LENGTH);
        if (!new PlayerEnterCombatEvent(attacker, tag).callEvent()) {
            return;
        }
        if (!new PlayerEnterCombatEvent(victim, tag).callEvent()) {
            return;
        }

        this.combatRepository.registerCombatTag(tag);
    }

}
