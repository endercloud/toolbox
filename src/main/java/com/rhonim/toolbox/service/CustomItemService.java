package com.rhonim.toolbox.service;

import com.google.auto.service.AutoService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.entities.customitems.CustomItem;
import com.rhonim.toolbox.entities.customitems.CustomItemInstance;
import com.rhonim.toolbox.injection.GuiceServiceLoader;
import com.rhonim.toolbox.injection.Service;
import com.rhonim.toolbox.repo.CustomItemRepository;
import com.rhonim.toolbox.util.ItemUtil;
import com.rhonim.toolbox.util.PlayerUtil;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import javax.annotation.Nullable;
import java.util.function.Consumer;

@Singleton
@AutoService(Service.class)
public class CustomItemService implements Service {
    private final NamespacedKey TYPE_KEY = new NamespacedKey(Toolbox.getInstance(), "customItemType");

    private final NamespacedKey DATA_KEY = new NamespacedKey(Toolbox.getInstance(), "customItemData");

    private final Gson gson;
    private final CustomItemRepository customItemRepository;
    private final Toolbox toolbox;

    @Inject
    public CustomItemService(Gson gson, CustomItemRepository customItemRepository, Toolbox toolbox) {
        this.gson = gson;
        this.customItemRepository = customItemRepository;
        this.toolbox = toolbox;
    }

    @Override
    public void init() {
        GuiceServiceLoader.load(CustomItem.class, toolbox.getClass().getClassLoader()).forEach(this.customItemRepository::registerCustomItem);
    }

    public <D extends CustomItem.Data, C extends CustomItem<D>> ItemStack getNewInstance(Class<C> type) {
        return this.getNewInstance(type, null);
    }

    public <D extends CustomItem.Data> ItemStack getNewInstance(Class<? extends CustomItem<D>> type, Consumer<D> updateFunction) {
        CustomItem<D> item = this.customItemRepository.getCustomItemFromType(type.getCanonicalName());

        if (item.getBaseType() == null) {
            throw new IllegalStateException("" + item.getClass().getName() + " does not have a base type set!");
        }
        ItemStack stack = new ItemStack(item.getBaseType());
        try {
            ItemUtil.applyData(stack, TYPE_KEY, type.getCanonicalName());
            this.setItemData(stack, type, item.getDataType().newInstance());

        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }

        this.updateItemData(stack, type, updateFunction);
        return stack;
    }

    public <D extends CustomItem.Data, C extends CustomItem<D>> D getItemData(ItemStack stack, Class<C> type) {
        CustomItem<D> item = this.customItemRepository.getCustomItemFromType(type.getCanonicalName());
        verifyCorrectType(stack, item);

        return this.gson.fromJson((String) ItemUtil.getData(stack, DATA_KEY, PersistentDataType.STRING), item.getDataType());
    }

    private void verifyCorrectType(ItemStack stack, CustomItem<?> type) {
        if (stack.getItemMeta() == null) {
            throw new IllegalArgumentException("Item type is not correct");
        }
        PersistentDataContainer dataContainer = stack.getItemMeta().getPersistentDataContainer();

        if (this.customItemRepository.getCustomItemFromType(dataContainer.get(TYPE_KEY, PersistentDataType.STRING)) != type) {
            throw new IllegalArgumentException("Item type is not correct");
        }
    }

    public <D extends CustomItem.Data> void updateItemData(ItemStack stack, Class<? extends CustomItem<D>> type, Consumer<D> updateFunction) {
        CustomItem<D> item = this.customItemRepository.getCustomItemFromType(type.getCanonicalName());
        verifyCorrectType(stack, item);

        D data = getItemData(stack, type);
        if (updateFunction != null) {
            updateFunction.accept(data);
        }
        setItemData(stack, type, data);
    }

    public <D extends CustomItem.Data, C extends CustomItem<D>> void setItemData(ItemStack stack, Class<C> type, D data) {
        CustomItem<D> item = this.customItemRepository.getCustomItemFromType(type.getCanonicalName());
        verifyCorrectType(stack, item);

        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(item.getName(data));
        meta.setLore(item.getLore(data));
        stack.setItemMeta(meta);

        if (data == null) {
            ItemUtil.removeData(stack, DATA_KEY);
        } else {
            ItemUtil.applyData(stack, DATA_KEY, this.gson.toJson(data));
        }
    }

    public <D extends CustomItem.Data> CustomItem<D> getCustomItem(ItemStack stack) {
        if (stack == null || stack.getType() == Material.AIR) {
            return null;
        }

        if (!ItemUtil.hasData(stack, TYPE_KEY, PersistentDataType.STRING)) {
            return null;
        }

        String type = ItemUtil.getData(stack, TYPE_KEY, PersistentDataType.STRING);

        CustomItem<D> item = this.customItemRepository.getCustomItemFromType(type);
        if (item == null) {
            throw new IllegalStateException("Invalid custom item type! Has something not loaded!? Type: " + type + " Item:" + item);
        }

        return item;
    }

    public <D extends CustomItem.Data> void handleInteract(PlayerInteractEvent event) {
        if (event.getItem() == null || event.getItem().getType().equals(Material.AIR)){
            return;
        }

        ItemStack item = event.getItem();

        CustomItem<D> customItem = this.getCustomItem(item);
        if (customItem == null) {
            return;
        }

        customItem.onInteractWithItem(event, new CustomItemInstance<>(  customItem, item));
    }

    public <D extends CustomItem.Data> void handlePlace(BlockPlaceEvent event) {
        ItemStack item = event.getItemInHand();

        CustomItem<D> customItem = this.getCustomItem(item);
        if (customItem == null) {
            return;
        }

        customItem.onPlaceItem(event, new CustomItemInstance<>(customItem, item));
    }

    public void handleGiveCommand(Player player, String itemName, int amount, @Nullable String meta) {
        CustomItem<?> ci = this.customItemRepository.getCustomItemFromName(itemName);
        if (ci == null) {
            Toolbox.getAudiences().player(player).sendMessage(Identity.nil(), Component.text("No item with the name \"" + itemName + "\" was found!", NamedTextColor.RED));
            return;
        }

        ItemStack itemStack = ci.generate(meta);

        if (itemStack != null) {
            for(int i = 0; i < amount; i++) {
                PlayerUtil.giveItemsSafely(player, itemStack);
            }
        } else {
            Toolbox.getAudiences().player(player).sendMessage(Identity.nil(), Component.text("There was an issue generating the ItemStack!", NamedTextColor.RED));
        }
    }
}
