package com.rhonim.toolbox.commands;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.service.CustomItemService;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

@Singleton
@AutoService(ToolboxCommand.class)
public class CustomItemCommand extends ToolboxCommand {
    @Inject
    private CustomItemService customItemService;

    public CustomItemCommand() {
        super("customitem");
        this.setPermission("toolbox.command.customitem");
        this.registerSubCommands(
                new GiveCommand()
        );
    }

    @Override
    public boolean run(CommandSender sender, String label, String[] args) {
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String label, String[] args) {
        List<String> list = new ArrayList<>();
        for (ToolboxCommand cmd : this.subCommands.values()) {
            if (cmd.canRun(sender)) {
                list.add(cmd.getLabel());
            }
        }
        return list;
    }

    private class GiveCommand extends ToolboxCommand {

        public GiveCommand() {
            super("give");
        }

        @Override
        public boolean run(CommandSender sender, String label, String[] args) {
            Audience audience = Toolbox.getAudiences().sender(sender);
            if (args.length < 3) {
                audience.sendMessage(Identity.nil(), Component.text("Invalid arguments!", NamedTextColor.RED));
                audience.sendMessage(Identity.nil(), Component.text("/customitem give <player> <item> <amount> [meta]"));
                return true;
            }

            Player player = Bukkit.getPlayer(args[0]);
            if (player == null) {
                audience.sendMessage(Identity.nil(), Component.text("Player not found!", NamedTextColor.RED));
                return true;
            }

            int amount;
            try {
                amount = Integer.parseInt(args[2]);
            } catch (NumberFormatException e) {
                audience.sendMessage(Identity.nil(), Component.text("Invalid amount provided.", NamedTextColor.RED));
                return true;
            }

            customItemService.handleGiveCommand(player, args[1],amount,  args.length >= 4 ? args[3] : "");

            return true;
        }
    }
}
