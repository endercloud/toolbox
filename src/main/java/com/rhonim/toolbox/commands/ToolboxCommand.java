package com.rhonim.toolbox.commands;

import com.google.common.base.Joiner;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.util.StringUtils;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ToolboxCommand extends Command {
    protected final Map<String, ToolboxCommand> subCommands;

    public ToolboxCommand(String name) {
        super(name);
        this.subCommands = new HashMap<>();
    }

    public ToolboxCommand(String name, String description, String usageMessage, List<String> aliases) {
        super(name, description, usageMessage, aliases);
        this.subCommands = new HashMap<>();
    }

    public abstract boolean run(CommandSender sender, String label, String[] args);

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        ToolboxCommand command;
        if (this.subCommands.isEmpty() || args.length <= 0 || (command = this.subCommands.get(args[0].toLowerCase())) == null) {
            return this.onTabComplete(sender, alias, args);
        } else {
            if (!this.canRun(sender)) {
                return super.tabComplete(sender, alias, args);
            }

            return command.onTabComplete(sender, alias, Arrays.copyOfRange(args, 1, args.length));
        }
    }

    public List<String> onTabComplete(CommandSender sender, String label, String[] args) {
        return null;
    }

    public boolean canRun(CommandSender sender) {
        return this.getPermission() == null || sender.hasPermission(this.getPermission());
    }

    protected List<TextComponent> getHelpText(CommandSender sender) {
        List<TextComponent> list = new ArrayList<>();
        for (ToolboxCommand command : this.subCommands.values()) {
            if (!this.canRun(sender)) {
                continue;
            }

            Toolbox.getAudiences().sender(sender).sendMessage(Identity.nil(), Component.text("/" + this.getLabel() + " " + command.getLabel()));
        }

        return list;
    }

    protected void sendHelp(CommandSender sender) {
        Audience audience = Toolbox.getAudiences().sender(sender);
        audience.sendMessage(Identity.nil(), Component.text("________" + StringUtils.toCamelCase(this.getName()) + " Help________", NamedTextColor.YELLOW, TextDecoration.BOLD)); //TODO Cleanup
        this.getHelpText(sender).forEach(c -> audience.sendMessage(Identity.nil(), c));
    }

    public void registerSubCommands(ToolboxCommand... commands) {
        for (ToolboxCommand command : commands) {
            this.subCommands.put(command.getName(), command);
        }
    }

    @Override
    public boolean execute(CommandSender sender, String label, String[] args) {
        if (this.subCommands.isEmpty() || args.length <= 0) {
            return this.run(sender, label, args);
        } else {
            ToolboxCommand command = this.subCommands.get(args[0].toLowerCase());
            Audience audience = Toolbox.getAudiences().sender(sender);
            if (command == null) {
                for (ToolboxCommand subCmd : this.subCommands.values()) {
                    if (subCmd.getAliases().contains(args[0].toLowerCase())) {
                        command = subCmd;
                    }
                }
            }

            if (command != null) {
                if (!this.canRun(sender)) {
                    audience.sendMessage(Identity.nil(), Component.text("You don't have permission to use this command."));
                    return true;
                }
                return command.run(sender, label, Arrays.copyOfRange(args, 1, args.length));
            }

            audience.sendMessage(Identity.nil(), Component.text("Invalid argument provided \"" + args[0] + "\"!", NamedTextColor.RED));
            audience.sendMessage(Identity.nil(), Component.text("Valid arguments: " + Joiner.on(", ").join(this.subCommands.keySet()), NamedTextColor.GRAY));

            return true;
        }
    }
}
