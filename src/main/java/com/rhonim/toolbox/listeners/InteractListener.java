package com.rhonim.toolbox.listeners;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.rhonim.toolbox.service.CustomItemService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

@Singleton
@AutoService(Listener.class)
public class InteractListener implements Listener {
    private final CustomItemService customItemService;

    @Inject
    public InteractListener(CustomItemService customItemService) {
        this.customItemService = customItemService;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        this.customItemService.handleInteract(e);
    }
}
