package com.rhonim.toolbox.listeners;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.rhonim.toolbox.service.CombatService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

@Singleton
@AutoService(Listener.class)
public class DamageListener implements Listener {
    private final CombatService combatService;

    @Inject
    public DamageListener(CombatService combatService) {
        this.combatService = combatService;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityDamageMonitor(EntityDamageByEntityEvent event) {
        this.combatService.handleDamageMonitor(event);
    }
}
