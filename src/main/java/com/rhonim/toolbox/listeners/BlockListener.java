package com.rhonim.toolbox.listeners;

import com.google.auto.service.AutoService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.rhonim.toolbox.service.CustomItemService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

@Singleton
@AutoService(Listener.class)
public class BlockListener implements Listener {

    private final CustomItemService customItemService;

    @Inject
    public BlockListener(CustomItemService customItemService) {
        this.customItemService = customItemService;
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        this.customItemService.handlePlace(event);
    }
}
