package com.rhonim.toolbox.injection;

public interface Service {
    public void init();
}