package com.rhonim.toolbox.injection;

import com.google.auto.service.AutoService;
import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;

@AutoService(Module.class)
public class LuckPermsModule extends AbstractModule {

    @Provides
    @Singleton
    LuckPerms getLuckPerms() {
        return LuckPermsProvider.get();
    }

}
