package com.rhonim.toolbox.injection;

import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.entities.providers.GsonProvider;
import org.bukkit.Bukkit;
import org.bukkit.Server;

public class ToolboxModule extends AbstractModule {

    private final Toolbox toolbox;

    public ToolboxModule(Toolbox toolbox) {
        this.toolbox = toolbox;
    }

    @Override
    protected void configure() {
        bind(Toolbox.class).toInstance(this.toolbox);
        bind(Gson.class).toProvider(GsonProvider.class);
        bind(Server.class).toInstance(Bukkit.getServer());
    }

}

