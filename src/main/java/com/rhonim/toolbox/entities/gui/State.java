package com.rhonim.toolbox.entities.gui;

import org.bukkit.entity.Player;

public class State {

    private final String id;
    private GUI gui;
    private Player player;

    public State(String id, GUI gui) {
        this.id = id;
        this.gui = gui;
        this.player = null;
    }

    public State(String id, GUI gui, Player player) {
        this.id = id;
        this.gui = gui;
        this.player = player;
    }

    public String getId() {
        return id;
    }

    public GUI getGui() {
        return gui;
    }

    public void setGui(GUI gui) {
        this.gui = gui;
    }

    public boolean hasPlayer() {
        return this.player != null;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
