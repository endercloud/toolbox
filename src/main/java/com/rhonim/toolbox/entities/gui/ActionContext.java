package com.rhonim.toolbox.entities.gui;

import com.rhonim.toolbox.Toolbox;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.event.inventory.InventoryClickEvent;

public class ActionContext extends Action {

    private UIRunnable runnable;

    public ActionContext(UIRunnable runnable) {
        this.runnable = runnable;
    }

    public void runAction(InventoryClickEvent event) {
        if (this.runnable != null) {
            this.runnable.run(event, this);
        } else {
            Toolbox.getAudiences().player(this.getPlayer()).sendMessage(Identity.nil(), Component.text("You can not run a null action!", NamedTextColor.RED));
        }
    }


}
