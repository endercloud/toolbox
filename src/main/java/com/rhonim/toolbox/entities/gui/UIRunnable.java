package com.rhonim.toolbox.entities.gui;

import org.bukkit.event.inventory.InventoryClickEvent;

public interface UIRunnable {
    void run(InventoryClickEvent event, final ActionContext context);
}
