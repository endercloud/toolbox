package com.rhonim.toolbox.entities.gui;

import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.util.Scheduler;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class Page extends State {

    private final String title;
    private final int rows;
    private final Map<Integer, Element> elements;
    private final Map<Integer, ErrorElement> errorElements = new HashMap<>();
    private final ProxyInventory inventory;
    private long updateTickDelay;
    private boolean autoUpdate;
    private boolean invCheckOverride;
    private boolean allowAdditionAndRemoval;
    private boolean allowDrag;
    private boolean allowShiftClicking;
    private boolean isSync;
    private ItemStack fillerStack;
    private Consumer<Page> prePopulateConsumer;
    private BiConsumer<Page, InventoryClickEvent> playerInvClickConsumer;
    private BiConsumer<Page, InventoryCloseEvent> invCloseConsumer;
    private BiConsumer<Page, InventoryDragEvent> dragItemConsumer;
    private boolean populated = false;
    private boolean eventsRegistered = false;
    private BukkitRunnable updaterTask = null;


    public Page(String id, GUI gui, String title, int rows, Map<Integer, Element> elements, boolean autoUpdate,
                long updateTickDelay, boolean invCheckOverride, boolean allowAdditionAndRemoval, boolean allowDrag, boolean allowShiftClicking,
                boolean isSync, ItemStack fillerStack, Consumer<Page> prePopulateConsumer, BiConsumer<Page, InventoryClickEvent> playerInvClickConsumer,
                BiConsumer<Page, InventoryCloseEvent> invCloseConsumer, BiConsumer<Page, InventoryDragEvent> dragItemConsumer) {
        super(id, gui);
        this.title = title;
        this.rows = rows;
        this.allowAdditionAndRemoval = allowAdditionAndRemoval;
        this.fillerStack = fillerStack;
        this.prePopulateConsumer = prePopulateConsumer;
        this.playerInvClickConsumer = playerInvClickConsumer;
        this.invCloseConsumer = invCloseConsumer;
        this.dragItemConsumer = dragItemConsumer;
        this.inventory = new ProxyInventory(Bukkit.createInventory(null, Math.min(9 * rows, 54), title), Bukkit.createInventory(null, Math.min(9 * rows, 54), title));
        this.elements = elements;
        this.autoUpdate = autoUpdate;
        this.updateTickDelay = updateTickDelay;
        this.invCheckOverride = invCheckOverride;
        this.allowDrag = allowDrag;
        this.allowShiftClicking = allowShiftClicking;
        this.isSync = isSync;
    }

    public InventoryView openPage(Player player) {
        if (!eventsRegistered) {
            Bukkit.getPluginManager().registerEvents(new GUIEvents(this), Toolbox.getInstance());
            eventsRegistered = true;
        }

        super.setPlayer(player);

        if (!this.populated) {
            this.populate();
        } else {
            this.repopulate();
        }

        if (this.autoUpdate && this.updaterTask == null) {
            this.updaterTask = new GUIUpdateTask();
            if (isSync) {
                this.updaterTask.runTaskTimer(Toolbox.getInstance(), 0, updateTickDelay);
            } else {
                this.updaterTask.runTaskTimerAsynchronously(Toolbox.getInstance(), 0, updateTickDelay);
            }
        }

        return player.openInventory(this.inventory.getMainInventory());
    }


    public void populate() {
        this.inventory.clear();
        if (this.prePopulateConsumer != null) {
            this.prePopulateConsumer.accept(this);
        }

        for (Map.Entry<Integer, Element> entry : this.elements.entrySet()) {
            Element element;
            if ((element = this.errorElements.get(entry.getKey())) == null) {
                element = entry.getValue();
            } else if(((ErrorElement) element).getExpire() < System.currentTimeMillis()) {
                this.errorElements.remove(entry.getKey());
            } else if (!this.autoUpdate) {
                long expire = ((ErrorElement) element).getExpire();
                if (expire > System.currentTimeMillis()) {
                    Scheduler.laterAsync(() -> {
                        this.errorElements.remove(entry.getKey());
                        this.repopulate();
                    }, (System.currentTimeMillis() - expire / 1000) * 20);
                }
            }
            this.inventory.setItem(entry.getKey(), element.getItem(this));
        }

        if (this.fillerStack != null) {
            for (int i = 0; i < this.inventory.getSize(); i++) {
                if (this.elements.containsKey(i)) {
                    continue;
                }

                this.inventory.setItem(i, this.fillerStack.clone());
            }
        }

        this.inventory.apply();
        this.populated = true;
    }

    protected final void setUpdateTicks(int ticks) {
        this.setUpdateTicks(ticks, false);
    }

    protected final void setUpdateTicks(int ticks, boolean isSync) {
        this.isSync = isSync;
        if (this.updaterTask == null) {
            return; //Inventory isn't open yet.. don't update it
        }

        this.updaterTask.cancel();
        this.updaterTask = null;

        this.updaterTask = new GUIUpdateTask();
        if (isSync) {
            this.updaterTask.runTaskTimer(Toolbox.getInstance(), 0, ticks);
        } else {
            this.updaterTask.runTaskTimerAsynchronously(Toolbox.getInstance(), 0, ticks);
        }
    }


    public void repopulate() {
        if (this.getPlayer() != null && !this.getPlayer().isOnline()) {
            this.close(true);
            return;
        }

        this.inventory.clear();
        if (this.prePopulateConsumer != null) {
            this.prePopulateConsumer.accept(this);
        }

        for (Map.Entry<Integer, Element> entry : this.elements.entrySet()) {
            Element element;
            if ((element = this.errorElements.get(entry.getKey())) == null) {
                element = entry.getValue();
            } else if(((ErrorElement) element).getExpire() < System.currentTimeMillis()) {
                this.errorElements.remove(entry.getKey());
            } else if (!this.autoUpdate) {
                long expire = ((ErrorElement) element).getExpire();
                if (expire > System.currentTimeMillis()) {
                    Scheduler.laterAsync(() -> {
                        this.errorElements.remove(entry.getKey());
                        this.repopulate();
                    }, (System.currentTimeMillis() - expire / 1000) * 20);
                }
            }
            this.inventory.setItem(entry.getKey(), element.getItem(this));
        }

        if (this.fillerStack != null) {
            for (int i = 0; i < this.inventory.getSize(); i++) {
                if (this.elements.containsKey(i)) {
                    continue;
                }

                this.inventory.setItem(i, this.fillerStack.clone());
            }
        }

        this.inventory.apply();
    }

    @Override
    public void setPlayer(Player player) {
        if (player == null) {
            return;
        }

        for (Element e : this.elements.values()) {
            if (e instanceof ActionableElement) {
                Action action = ((ActionableElement) e).getAction();
                action.setPlayer(player);
                action.setGui(this.getGui());
                action.setState(this);
            }
        }

        super.setPlayer(player);
    }

    public void close(boolean forceClose) {
        if (this.updaterTask != null) {
            this.updaterTask.cancel();
            this.updaterTask = null;
        }

        if (forceClose && this.hasPlayer() && this.getPlayer().isOnline()) {
            this.getPlayer().closeInventory();
            this.setPlayer(null);
        }
    }

    public ProxyInventory getInventory() {
        return inventory;
    }

    private class GUIUpdateTask extends BukkitRunnable {
        @Override
        public void run() {
            try {
                repopulate();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public void putError(int slot, String errorTitle, List<String> errorDescription) {
        this.putError(slot,errorTitle, errorDescription, System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(2L));
    }

    public void putError(int slot, String errorTitle, List<String> errorDescription, long expire) {
        this.putElement(slot, new ErrorElement(errorTitle, errorDescription, expire));
        this.repopulate();
    }

    public void putElement(int slot, Element element) {
        if (element instanceof ErrorElement) {
            this.errorElements.put(slot, (ErrorElement) element);
        } else {
            this.elements.put(slot, element);
        }

        if (element instanceof ActionableElement) {
            Action action = ((ActionableElement) element).getAction();
            if (this.getPlayer() != null) {
                action.setPlayer(this.getPlayer());
            }
            action.setState(this);
            action.setGui(this.getGui());
        }
    }

    public Element getElement(Integer slot) {
        return this.elements.get(slot);
    }

    public void removeElement(Integer slot) {
        this.elements.remove(slot);
    }

    private class GUIEvents implements Listener {
        private final Page page;

        protected GUIEvents(Page page) {
            this.page = page;
        }

        @EventHandler
        public void onInventoryClick(InventoryClickEvent event) {
            try {
                if (this.page.getInventory().getMainInventory().getViewers().contains(event.getWhoClicked())) {
                    List<InventoryAction> deniedActions = new ArrayList<>(Arrays.asList(
                            InventoryAction.CLONE_STACK,
                            InventoryAction.COLLECT_TO_CURSOR,
                            InventoryAction.UNKNOWN
                    ));

                    if (!allowAdditionAndRemoval && !allowShiftClicking) {
                        deniedActions.add(InventoryAction.MOVE_TO_OTHER_INVENTORY);
                    }

                    if (deniedActions.contains(event.getAction())) {
                        event.setCancelled(true);
                    }

                    if (!allowShiftClicking && event.getClick().isShiftClick()) {
                        event.setCancelled(true);
                    }

                    if (!invCheckOverride && event.getClickedInventory() == null) {
                        return;
                    }

                    if (event.getClickedInventory() != null && !event.getClickedInventory().equals(this.page.getInventory().getMainInventory())) {
                        //Player inventory was clicked
                        if (this.page.playerInvClickConsumer != null) {
                            this.page.playerInvClickConsumer.accept(this.page, event);
                        }
                        return;
                    }

                    if (allowAdditionAndRemoval) {
                        return;
                    }

                    event.setCancelled(true);

                    if (!(event.getWhoClicked() instanceof Player)) {
                        return;
                    }

                    int raw = event.getRawSlot();
                    Element element = this.page.elements.get(raw);
                    if (element instanceof ActionableElement) {
                        ((ActionableElement) element).getAction().runAction(event);
                    }
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }

        @EventHandler
        public void onInventoryClose(InventoryCloseEvent event) {
            if (!event.getInventory().equals(this.page.getInventory().getMainInventory())) {
                return;
            }

            try {
                if (this.page.invCloseConsumer != null) {
                    this.page.invCloseConsumer.accept(this.page, event);
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }

            if (this.page.getInventory().getMainInventory().getViewers().size() <= 1) {
                //this.page.inventory.clear();

                if (eventsRegistered) {
                    HandlerList.unregisterAll(this);
                    eventsRegistered = false;
                }

                if (this.page.updaterTask != null) {
                    this.page.updaterTask.cancel();
                    this.page.updaterTask = null;
                }
            }
        }

        @EventHandler
        public void onInventoryDrag(InventoryDragEvent event) {
            try {
                if (!event.getInventory().equals(this.page.getInventory().getMainInventory())) {
                    return;
                }
                if (!allowDrag) {
                    event.setCancelled(true);
                } else {
                    if (this.page.dragItemConsumer != null) {
                        this.page.dragItemConsumer.accept(page, event);
                    }
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public String getTitle() {
        return title;
    }

    public int getRows() {
        return rows;
    }

    public static PageBuilder builder() {
        return new PageBuilder();
    }

    public static class PageBuilder {
        private final Map<Integer, Element> elements;

        private GUI parent;
        private int rows;
        private String title;
        private boolean autoUpdate;
        private long updateTickDelay;
        private boolean overrideInvCheck;
        private boolean allowAdditionAndRemoval;
        private boolean allowDrag;
        private boolean allowShiftClicking;
        private boolean isSync;
        private ItemStack fillerStack;
        private Consumer<Page> prePopulateConsumer;
        private BiConsumer<Page, InventoryClickEvent> playerInvClickConsumer;
        private BiConsumer<Page, InventoryCloseEvent> closeInvConsumer;
        private BiConsumer<Page, InventoryDragEvent> invDragConsumer;

        private PageBuilder() {
            this.elements = new HashMap<>();
            this.parent = null;
            this.rows = 6;
            this.title = "Unnamed Page";
            this.autoUpdate = false;
            this.updateTickDelay = 20;
            this.overrideInvCheck = false;
            this.allowDrag = false;
            this.allowShiftClicking = false;
            this.isSync = false;
            this.fillerStack = null;
            this.prePopulateConsumer = null;
            this.playerInvClickConsumer = null;
            this.closeInvConsumer = null;
            this.invDragConsumer = null;
        }

        public PageBuilder putElement(int slot, Element element) {
            this.elements.put(slot, element);
            return this;
        }

        public PageBuilder addElement(Element element) {
            this.elements.put(this.elements.size(), element);
            return this;
        }

        public PageBuilder setParent(GUI parent) {
            this.parent = parent;
            return this;
        }

        public PageBuilder setRows(int rows) {
            this.rows = rows;
            return this;
        }

        public PageBuilder setTitle(String title) {
            this.title = title;
            return this;
        }

        public PageBuilder setPrePopulateConsumer(Consumer<Page> prePopulateConsumer) {
            this.prePopulateConsumer = prePopulateConsumer;
            return this;
        }

        public PageBuilder setAllowAdditionAndRemoval(boolean allowRemoval) {
            this.allowAdditionAndRemoval = allowRemoval;
            return this;
        }

        public PageBuilder setAutoUpdate(boolean autoUpdate) {
            this.autoUpdate = autoUpdate;
            return this;
        }

        public PageBuilder setUpdateTickDelay(long updateTickDelay) {
            this.updateTickDelay = updateTickDelay;
            if (updateTickDelay >= 0) {
                this.autoUpdate = true;
            }
            return this;
        }

        public PageBuilder setOverrideInvCheck(boolean overrideInvCheck) {
            this.overrideInvCheck = overrideInvCheck;
            return this;
        }

        public PageBuilder setAllowDrag(boolean allowDrag) {
            this.allowDrag = allowDrag;
            return this;
        }

        public PageBuilder setAllowShiftClick(boolean allowShiftClicking) {
            this.allowShiftClicking = allowShiftClicking;
            return this;
        }

        public PageBuilder setSync(boolean isSync) {
            this.isSync = isSync;
            return this;
        }

        public PageBuilder setFillerItem(ItemStack stack) {
            this.fillerStack = stack;
            return this;
        }

        public PageBuilder setPlayerInvClick(BiConsumer<Page, InventoryClickEvent> playerInvClickConsumer) {
            this.playerInvClickConsumer = playerInvClickConsumer;
            return this;
        }

        public PageBuilder setCloseInv(BiConsumer<Page, InventoryCloseEvent> closeInvConsumer) {
            this.closeInvConsumer = closeInvConsumer;
            return this;
        }

        public PageBuilder setInvDrag(BiConsumer<Page, InventoryDragEvent> dragInvConsumer) {
            this.invDragConsumer = dragInvConsumer;
            return this;
        }

        public Page build(String id) {
            return new Page(id, this.parent, this.title, this.rows, this.elements, this.autoUpdate, this.updateTickDelay,
                    this.overrideInvCheck, this.allowAdditionAndRemoval, this.allowDrag, this.allowShiftClicking, this.isSync, this.fillerStack,
                    this.prePopulateConsumer, this.playerInvClickConsumer, this.closeInvConsumer, this.invDragConsumer);
        }
    }

    public class ProxyInventory implements Inventory {
        private Inventory mainInventory;
        private Inventory proxyInventory;

        private ProxyInventory (Inventory mainInventory, Inventory proxyInventory) {
            this.mainInventory = mainInventory;
            this.proxyInventory = proxyInventory;
        }

        public void apply() {
            this.mainInventory.setContents(this.proxyInventory.getContents());
        }

        @Override
        public int getSize() {
            return this.proxyInventory.getSize();
        }

        @Override
        public int getMaxStackSize() {
            return this.proxyInventory.getMaxStackSize();
        }

        @Override
        public void setMaxStackSize(int i) {
            this.proxyInventory.setMaxStackSize(i);
        }


        @Override
        public ItemStack getItem(int i) {
            return this.proxyInventory.getItem(i);
        }

        @Override
        public void setItem(int i, ItemStack itemStack) {
            this.proxyInventory.setItem(i, itemStack);
        }

        @Override
        public HashMap<Integer, ItemStack> addItem(ItemStack... itemStacks) throws IllegalArgumentException {
            return this.proxyInventory.addItem(itemStacks);
        }

        @Override
        public HashMap<Integer, ItemStack> removeItem(ItemStack... itemStacks) throws IllegalArgumentException {
            return this.proxyInventory.removeItem(itemStacks);
        }

        @Override
        public HashMap<Integer, ItemStack> removeItemAnySlot(ItemStack... itemStacks) throws IllegalArgumentException {
            return this.proxyInventory.removeItemAnySlot(itemStacks);
        }

        @Override
        public ItemStack[] getContents() {
            return this.proxyInventory.getContents();
        }

        @Override
        public void setContents(ItemStack[] itemStacks) throws IllegalArgumentException {
            this.proxyInventory.setContents(itemStacks);
        }

        @Override
        public ItemStack[] getStorageContents() {
            return this.proxyInventory.getStorageContents();
        }

        @Override
        public void setStorageContents(ItemStack[] itemStacks) throws IllegalArgumentException {
            this.proxyInventory.setStorageContents(itemStacks);
        }

        @Override
        public boolean contains(Material material) throws IllegalArgumentException {
            return this.proxyInventory.contains(material);
        }

        @Override
        public boolean contains(ItemStack itemStack) {
            return this.proxyInventory.contains(itemStack);
        }


        @Override
        public boolean contains(Material material, int i) throws IllegalArgumentException {
            return this.proxyInventory.contains(material, i);
        }

        @Override
        public boolean contains(ItemStack itemStack, int i) {
            return this.proxyInventory.contains(itemStack, i);
        }

        @Override
        public boolean containsAtLeast(ItemStack itemStack, int i) {
            return this.proxyInventory.containsAtLeast(itemStack, i);
        }


        @Override
        public HashMap<Integer, ? extends ItemStack> all(Material material) throws IllegalArgumentException {
            return this.proxyInventory.all(material);
        }

        @Override
        public HashMap<Integer, ? extends ItemStack> all(ItemStack itemStack) {
            return this.proxyInventory.all(itemStack);
        }

        @Override
        public int first(Material material) throws IllegalArgumentException {
            return this.proxyInventory.first(material);
        }

        @Override
        public int first(ItemStack itemStack) {
            return this.proxyInventory.first(itemStack);
        }

        @Override
        public int firstEmpty() {
            return this.proxyInventory.firstEmpty();
        }

        @Override
        public boolean isEmpty() {
            return this.proxyInventory.isEmpty();
        }

        @Override
        public void remove(Material material) throws IllegalArgumentException {
            this.proxyInventory.remove(material);
        }

        @Override
        public void remove(ItemStack itemStack) {
            this.proxyInventory.remove(itemStack);
        }

        @Override
        public void clear(int i) {
            this.proxyInventory.clear(i);
        }

        @Override
        public void clear() {
            this.proxyInventory.clear();
        }

        @Override
        public List<HumanEntity> getViewers() {
            return this.mainInventory.getViewers();
        }

        @Override
        public InventoryType getType() {
            return this.mainInventory.getType();
        }

        @Override
        public InventoryHolder getHolder() {
            return this.mainInventory.getHolder();
        }

        @Override
        public InventoryHolder getHolder(boolean b) {
            return null;
        }

        @Override
        public ListIterator<ItemStack> iterator() {
            return this.proxyInventory.iterator();
        }

        @Override
        public ListIterator<ItemStack> iterator(int i) {
            return this.proxyInventory.iterator(i);
        }

        @Override
        public Location getLocation() {
            return null;
        }

        public Inventory getMainInventory() {
            return mainInventory;
        }

        public Inventory getProxyInventory() {
            return proxyInventory;
        }
    }
}
