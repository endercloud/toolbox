package com.rhonim.toolbox.entities.gui;

import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.util.Scheduler;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class GUI {
    private final Map<String, State> states;

    private String initialState;

    public GUI() {
        this.states = new HashMap<>();
    }

    public GUI(Map<String, State> states) {
        this.states = states;
    }

    public GUI(String initialState, Map<String, State> states) {
        this.initialState = initialState;
        this.states = states;
    }

    public void addState(State state) {
        state.setGui(this);

        if (this.hasState(state.getId())) {
            throw new IllegalStateException("A State with ID \"" + state.getId() + "\" already exists in this GUI");
        }

        if (this.initialState == null) {
            this.initialState = state.getId();
        }

        this.states.put(state.getId(), state);
    }

    public void removeState(String id) {
        if (this.initialState != null && this.initialState.equalsIgnoreCase(id)) {
            this.initialState = null;
        }

        this.states.remove(id);
    }

    public void setInitialState(State state) {
        this.initialState = state.getId();

        if (!this.hasState(state.getId())) {
            this.addState(state);
        }
    }

    public void open(Player player) {
        this.openState(player, this.initialState);
    }

    public void openState(Player player, String id) {
        if (!Bukkit.isPrimaryThread()) { //Unfortunatly we need to open inventories main thread
            Scheduler.sync(() -> this.openState(player, id));
            return;
        }

        State state = this.states.get(id);
        if (state == null) {
            Toolbox.getAudiences().player(player).sendMessage(Identity.nil(), Component.text("Failed to open state ID " + id, NamedTextColor.RED));
            player.closeInventory();
            return;
        }

        state.setPlayer(player);

        if (state instanceof Page) {
            Page page = (Page) state;
            page.openPage(player);

            return;
        }

        player.closeInventory();
        Toolbox.getAudiences().player(player).sendMessage(Identity.nil(), Component.text("Attempted to open invalid state!", NamedTextColor.RED));
    }

    public boolean hasState(String id) {
        return this.states.get(id) != null;
    }

    public State getState(String id) {
        return this.states.get(id);
    }

    public String getInitialState() {
        return initialState;
    }
}
