package com.rhonim.toolbox.entities.gui;

import com.rhonim.toolbox.util.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;

import java.util.List;

public class ErrorElement extends Element{
    private final long expire;

    public ErrorElement(String errorTitle, List<String> errorDescription, long expire) {
        super(new ItemBuilder(Material.BARRIER)
                .withCustomName("" + ChatColor.RED + errorTitle)
                .withLore(errorDescription)
                .build());

        this.expire = expire;
    }

    public long getExpire() {
        return expire;
    }
}
