package com.rhonim.toolbox.entities.gui;

import org.bukkit.inventory.ItemStack;

import java.util.function.Function;

public class ActionableElement extends Element {

    private final ActionContext action;

    public ActionableElement(ItemStack item, ActionContext action) {
        super(item);

        this.action = action;
    }

    public ActionableElement(Function<State, ItemStack> stackFunction, ActionContext action) {
        super(stackFunction);
        this.action = action;
    }

    public Action getAction() {
        return action;
    }
}
