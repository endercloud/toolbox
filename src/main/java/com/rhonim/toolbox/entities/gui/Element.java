package com.rhonim.toolbox.entities.gui;

import org.bukkit.inventory.ItemStack;

import java.util.function.Function;

public class Element {

    private ItemStack item;
    private Function<State, ItemStack> stackFunction = null;

    public Element(ItemStack item) {
        this.item = item;
    }

    public Element(Function<State, ItemStack> stackFunction) {
        this.stackFunction = stackFunction;
        this.item = null;
    }

    public ItemStack getItem() {
        return this.item;
    }

    public ItemStack getItem(Page page) {
        if (this.stackFunction != null) {
            this.item = this.stackFunction.apply(page);
        }
        return item;
    }

    public Function<State, ItemStack> getStackFunction() {
        return stackFunction;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }
}
