package com.rhonim.toolbox.entities;

import java.util.UUID;

public class CombatTag {
    private final UUID attacker;
    private final UUID victim;
    private final long tagStart;
    private long tagExpire;
    private double attackerDamageDone = 0.0F;
    private double victimDamageDone = 0.0F;

    public CombatTag(UUID attacker, UUID victim, long tagStart, long tagExpire) {
        this.attacker = attacker;
        this.victim = victim;
        this.tagStart = tagStart;
        this.tagExpire = tagExpire;
    }

    public UUID getAttacker() {
        return attacker;
    }

    public UUID getVictim() {
        return victim;
    }

    public boolean isActive() {
        return System.currentTimeMillis() < this.tagExpire;
    }

    public long getTagStart() {
        return tagStart;
    }

    public long getTagExpire() {
        return tagExpire;
    }

    public void setTagExpire(long tagExpire) {
        this.tagExpire = tagExpire;
    }

    public void incrementAttackerDamage(double damage) {
        this.attackerDamageDone += damage;
    }

    public void incrementVictimDamage(double damage) {
        this.victimDamageDone += damage;
    }

    public double getAttackerDamageDone() {
        return attackerDamageDone;
    }

    public double getVictimDamageDone() {
        return victimDamageDone;
    }
}
