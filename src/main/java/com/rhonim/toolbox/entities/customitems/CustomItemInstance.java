package com.rhonim.toolbox.entities.customitems;

import com.google.inject.Inject;
import com.rhonim.toolbox.service.CustomItemService;
import org.bukkit.inventory.ItemStack;

public class CustomItemInstance<D extends CustomItem.Data> {

    @Inject
    private CustomItemService customItemService;
    private final CustomItem<D> item;
    private final ItemStack stack;
    private D data;

    public CustomItemInstance(CustomItem<D> item, ItemStack stack) {
        this.item = item;
        this.stack = stack;
    }

    public CustomItem<D> getItem() {
        return this.item;
    }

    public ItemStack getStack() {
        return this.stack;
    }

    public D getData() {
        if (data == null) {
            this.data = (D) customItemService.getItemData(this.stack, item.getClass());
        }

        return data;
    }

    public void setData(D data) {
        this.data = data;
        this.customItemService.setItemData(stack, item.getClass(), data);
    }

}
