package com.rhonim.toolbox.entities.customitems;

import com.google.inject.Inject;
import com.rhonim.toolbox.service.CustomItemService;
import org.bukkit.Material;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public abstract class CustomItem<D extends CustomItem.Data> {
    @Inject
    private CustomItemService customItemService;

    public abstract Material getBaseType();

    public abstract Class<D> getDataType();

    public abstract String getName(D data);

    public abstract List<String> getLore(D data);

    public abstract void onInteractWithItem(PlayerInteractEvent event, CustomItemInstance<D> item);

    public abstract void onPlaceItem(BlockPlaceEvent event, CustomItemInstance<D> item);

    public interface Data {

    }

    public String getCmdName() {
        return this.getClass().getSimpleName();
    }

    /*
    Meta is not used here, but can be used when overriden
     */
    public ItemStack generate(String meta) {
        return this.customItemService.getNewInstance(this.getClass());
    }
}
