package com.rhonim.toolbox.entities.events;

import com.rhonim.toolbox.entities.CombatTag;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerEnterCombatEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private final Player player;
    private final CombatTag combatTag;
    private boolean cancelled = false;

    public PlayerEnterCombatEvent(Player player, CombatTag combatTag) {
        this.player = player;
        this.combatTag = combatTag;
    }

    public Player getPlayer() {
        return player;
    }

    public CombatTag getCombatTag() {
        return combatTag;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
