package com.rhonim.toolbox.entities;

import com.google.gson.JsonObject;

public interface Configurable<C> {

    public String getConfigKey();
    public Class<C> getConfigType();
    public void configure(C config);
    public void setup(JsonObject object);

}
