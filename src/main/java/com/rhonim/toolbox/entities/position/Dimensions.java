package com.rhonim.toolbox.entities.position;

import com.google.common.collect.ImmutableList;
import com.rhonim.toolbox.util.RandomUtil;
import com.rhonim.toolbox.util.tuple.Pair;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.*;

public class Dimensions {

    /**
     * Minimum {@link Position}
     */
    private Position minimum;
    /**
     * Maximum {@link Position}
     */
    private Position maximum;

    public Dimensions(Position minimum, Position maximum) {
        double minX = Math.min(minimum.getX(), maximum.getX());
        double minY = Math.min(minimum.getY(), maximum.getY());
        double minZ = Math.min(minimum.getZ(), maximum.getZ());

        double maxX = Math.max(minimum.getX(), maximum.getX());
        double maxY = Math.max(minimum.getY(), maximum.getY());
        double maxZ = Math.max(minimum.getZ(), maximum.getZ());

        this.minimum = new Position(minX, minY, minZ);
        this.maximum = new Position(maxX, maxY, maxZ);
    }

    /**
     * @param position Position to check
     * @return If the provided Position is within the {@link Dimensions}
     */
    public boolean isWithinBounds(Position position) {
        double x = position.getX();
        double y = position.getY();
        double z = position.getZ();

        return x >= this.minimum.getX() && x <= this.maximum.getX() && y >= this.minimum.getY() && y <= this.maximum.getY() && z >= this.minimum.getZ() && z <= this.maximum.getZ();
    }

    public boolean isWithinBlockBounds(Position position) {
        double x = position.getBlockX();
        double y = position.getBlockY();
        double z = position.getBlockZ();

        return x >= this.minimum.getBlockX() && x <= this.maximum.getBlockX() && y >= this.minimum.getBlockY() && y <= this.maximum.getBlockY() && z >= this.minimum.getBlockZ() && z <= this.maximum.getBlockZ();
    }

    /**
     * @return all positions within these dimensions
     */
    public List<Position> getPositionsWithin() {
        List<Position> positions = new ArrayList<>();

        for (int x = getMinimum().getBlockX(); x <= getMaximum().getBlockX(); x++) {
            for (int y = getMinimum().getBlockY(); y <= getMaximum().getBlockY(); y++) {
                for (int z = getMinimum().getBlockZ(); z <= getMaximum().getBlockZ(); z++) {
                    positions.add(new Position(x, y, z));
                }
            }
        }

        return positions;
    }

    /**
     * @param position Position to check
     * @return If the provided Position is within the X and Z values of the {@link Dimensions}
     */
    public boolean isWithinBoundsXZ(Position position) {
        double x = position.getX();
        double z = position.getZ();

        return x >= this.minimum.getX() && x <= this.maximum.getX() && z >= this.minimum.getZ() && z <= this.maximum.getZ();
    }

    public boolean isWithinBoundsXZ(Dimensions dimensions) {
        for (Position position : dimensions.getCorners()) {
            if (!this.isWithinBoundsXZ(position)) {
                return false;
            }
        }

        return true;
    }


    /**
     * Expand the Dimension
     *
     * @param x Amount X to expand by
     * @param y Amount Y to expand by
     * @param z Amount Z to expand by
     * @return A new {@link Dimensions}
     */
    public Dimensions grow(double x, double y, double z) {
        return new Dimensions(this.minimum.subtract(x, y, z), this.maximum.add(x, y, z));
    }

    public Dimensions shift(int amount, BlockFace face) {
        switch (face) {
            case NORTH:
                return new Dimensions(this.minimum.subtract(0, 0, amount), this.maximum.subtract(0, 0, amount));
            case EAST:
                return new Dimensions(this.minimum.add(amount, 0, 0), this.maximum.add(amount, 0, 0));
            case SOUTH:
                return new Dimensions(this.minimum.add(0, 0, amount), this.maximum.add(0, 0, amount));
            case WEST:
                return new Dimensions(this.minimum.subtract(amount, 0, 0), this.maximum.subtract(amount, 0, 0));
            case UP:
                return new Dimensions(this.minimum.add(0, amount, 0), this.maximum.add(0, amount, 0));
            case DOWN:
                return new Dimensions(this.minimum.subtract(0, amount, 0), this.maximum.subtract(0, amount, 0));
            default:
                return this;
        }
    }

    /**
     * Shrink the Dimension
     *
     * @param x Amount X to expand by
     * @param y Amount Y to expand by
     * @param z Amoutn Z to expand by
     * @return A new {@link Dimensions}
     */
    public Dimensions shrink(double x, double y, double z) {
        return new Dimensions(this.minimum.add(x, y, z), this.maximum.subtract(x, y, z));
    }

    /**
     * Flatten the dimension to one block at Y
     *
     * @param y value to flatten to
     * @return A dimension
     */
    public Dimensions flatten(int y) {
        Position newMin = new Position(this.minimum.getBlockX(), y, this.minimum.getBlockZ());
        Position newMax = new Position(this.maximum.getBlockX(), y, this.maximum.getBlockZ());
        return new Dimensions(newMin, newMax);
    }

    /**
     * Check if Dimensions collide
     *
     * @param b Dimension to compate to
     * @return A boolean
     */
    public boolean collidesWith(Dimensions b) {
        Position minA = this.minimum;
        Position maxA = this.maximum;
        Position minB = b.getMinimum();
        Position maxB = b.getMaximum();

        return minA.getBlockX() <= maxB.getBlockX() && maxA.getBlockX() >= minB.getBlockX() &&
                minA.getBlockY() <= maxB.getBlockY() && maxA.getBlockY() >= minB.getBlockY() &&
                minA.getBlockZ() <= maxB.getBlockZ() && maxA.getBlockZ() >= minB.getBlockZ();
    }

    public Position getRandomPositionWithin() {
        int randomX = RandomUtil.getRandomNumberBetween(this.minimum.getBlockX(), this.maximum.getBlockX());
        int randomY = RandomUtil.getRandomNumberBetween(this.minimum.getBlockY(), this.maximum.getBlockY());
        int randomZ = RandomUtil.getRandomNumberBetween(this.minimum.getBlockZ(), this.maximum.getBlockZ());
        return new Position(randomX, randomY, randomZ);
    }

    public ImmutableList<Block> getBottomSurface(World world) {
        ImmutableList.Builder<Block> list = new ImmutableList.Builder<>();

        for (int x = getMinimum().getBlockX(); x <= getMaximum().getBlockX(); x++) {
            for (int z = getMinimum().getBlockZ(); z <= getMaximum().getBlockZ(); z++) {
                list.add(world.getBlockAt(x, getMinimum().getBlockY(), z));
            }
        }

        return list.build();
    }

    public ImmutableList<Position> getCorners() {
        ImmutableList.Builder<Position> list = new ImmutableList.Builder<>();

        double[] xVals = {this.maximum.getX(), this.minimum.getX()};
        double[] yVals = {this.maximum.getY(), this.minimum.getY()};
        double[] zVals = {this.maximum.getZ(), this.minimum.getZ()};

        if (this.minimum.getY() == this.maximum.getY()) {
            for (double x : xVals) {
                for (double z : zVals) {
                    list.add(new Position(x, this.minimum.getY(), z));
                }
            }
        } else if (this.minimum.getX() == this.maximum.getX()) {
            for (double y : yVals) {
                for (double z : zVals) {
                    list.add(new Position(this.minimum.getX(), y, z));
                }
            }
        } else if (this.minimum.getZ() == this.maximum.getZ()) {
            for (double x : xVals) {
                for (double y : yVals) {
                    list.add(new Position(x, y, this.minimum.getZ()));
                }
            }
        } else {
            for (double x : xVals) {
                for (double y : yVals) {
                    for (double z : zVals) {
                        list.add(new Position(x, y, z));
                    }
                }
            }
        }

        return list.build();
    }

    /*public Dimensions rotateToMatch(Rotation rotation) {
        //Subtract 1 from length and width since the position is within the building

        switch (rotation) {
            default:
            case NONE:
                return new Dimensions(getMinimum(), getMinimum().add(getSizeX() - 1, getSizeY(), getSizeZ() - 1));
            case DEGREES_90:
                return new Dimensions(getMinimum(), getMinimum().add(this.getSizeZ() - 1, this.getSizeY(), -(this.getSizeX() - 1)));
            case DEGREES_180:
                return new Dimensions(getMinimum(), getMinimum().add(-(this.getSizeX() - 1), this.getSizeY(), -(this.getSizeZ() - 1)));
            case DEGREES_270:
                return new Dimensions(getMinimum(), getMinimum().add(-(this.getSizeZ() - 1), this.getSizeY(), this.getSizeX() - 1));
        }
    }

    public void visualize(ParticleEffect effect, ParticleEffect.OrdinaryColor color, World world, Player player) {
        this.visualize(effect, color, world, player, 16);
    }

    public void visualize(ParticleEffect effect, ParticleEffect.OrdinaryColor color, World world, Player player, int range) {
        int minx = this.minimum.getBlockX();
        int maxx = this.maximum.getBlockX();
        int miny = this.minimum.getBlockY();
        int maxy = this.maximum.getBlockY();
        int minz = this.minimum.getBlockZ();
        int maxz = this.maximum.getBlockZ();

        Position playerPos = player == null ? null : new Position(player.getLocation());

        for (int x = minx; x <= maxx; x++) {
            for (int y = miny; y <= maxy; y++) {
                for (int z = minz; z <= maxz; z++) {
                    if (y == miny || y == maxy || y % 5 == 0) {
                        WorldPosition worldPosition = new WorldPosition(world, x + 0.5, y + 0.5, z + 0.5);
                        if (playerPos != null && playerPos.distanceSquared(worldPosition) > range * range) {
                            continue;
                        }

                        if (x == minx || z == minz || x == maxx || z == maxz) {
                            if (effect.equals(ParticleEffect.RED_DUST)) {
                                effect.display(color, worldPosition, player);
                            } else if (player == null) {
                                effect.display(worldPosition, 0.0F, 0.0F, 0.0F, 0, 1);
                            } else {
                                effect.display(worldPosition, 0.0F, 0.0F, 0.0F, 0, 1, player);
                            }
                        }
                    } else {
                        if (x == minx && z == minz || x == maxx && z == minz
                                || x == minx && z == maxz || x == maxx && z == maxz) {
                            WorldPosition worldPosition = new WorldPosition(world, x + 0.5, y + 0.5, z + 0.5);
                            if (playerPos != null && playerPos.distanceSquared(worldPosition) > range * range) {
                                continue;
                            }

                            if (effect.equals(ParticleEffect.RED_DUST)) {
                                effect.display(color, worldPosition, player);
                            } else if (player == null) {
                                effect.display(worldPosition, 0.0F, 0.0F, 0.0F, 0, 1);
                            } else {
                                effect.display(worldPosition, 0.0F, 0.0F, 0.0F, 0, 1, player);
                            }
                        }
                    }
                }
            }
        }
    }

    public void visualize(World world) {
        this.visualize(ParticleEffect.HAPPY_VILLAGER, null, world, null);
    }*/

    /**
     * Get a set of all the chunks in island
     *
     * @return Set of Pairs
     */
    public Set<Pair<Integer, Integer>> getChunksAsCoords(World world) {
        int minX = this.minimum.getChunkX();
        int minZ = this.minimum.getChunkZ();
        int maxX = this.maximum.getChunkX();
        int maxZ = this.maximum.getChunkZ();

        Set<Pair<Integer, Integer>> chunkPairs = new HashSet<>();
        for (int x = minX; x <= maxX; x++) {
            for (int z = minZ; z <= maxZ; z++) {
                chunkPairs.add(Pair.of(x, z));
            }
        }
        return chunkPairs;
    }

    public int getSizeX() {
        return Math.abs(this.maximum.getBlockX() - this.minimum.getBlockX());
    }

    public int getSizeY() {
        return Math.abs(this.maximum.getBlockY() - this.minimum.getBlockY());
    }

    public int getSizeZ() {
        return Math.abs(this.maximum.getBlockZ() - this.minimum.getBlockZ());
    }


    /**
     * @return Minimum {@link Position}
     */
    public Position getMinimum() {
        return this.minimum;
    }


    /**
     * @return Maximum {@link Position}
     */
    public Position getMaximum() {
        return this.maximum;
    }


    @Override
    public String toString() {
        return "Dimensions[" + this.minimum + " : " + this.maximum + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dimensions that = (Dimensions) o;
        return Objects.equals(minimum, that.minimum) &&
                Objects.equals(maximum, that.maximum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(minimum, maximum);
    }
}



