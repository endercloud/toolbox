package com.rhonim.toolbox.entities.position;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

public class WorldPosition extends Position {
    private String worldName;

    public WorldPosition(Location location) {
        super(location);
        this.worldName = location.getWorld().getName();
    }

    public WorldPosition(World world, double x, double y, double z) {
        super(x, y, z);
        this.worldName = world.getName();
    }

    public WorldPosition(World world, double x, double y, double z, float yaw, float pitch) {
        super(x, y, z, yaw, pitch);
        this.worldName = world.getName();
    }

    public WorldPosition(String worldName, double x, double y, double z, float yaw, float pitch) {
        super(x, y, z, yaw, pitch);
        this.worldName = worldName;
    }

    public WorldPosition(World world, Position position) {
        super(position.getX(), position.getY(), position.getZ(), position.getYaw(), position.getPitch());
        this.worldName = world.getName();
    }

    public WorldPosition(String string) {
        super(string.split(":")[1]);
        this.worldName = string.split(":")[0];
    }

    public World getWorld() {
        return Bukkit.getWorld(worldName);
    }

    public String getWorldName() {
        return this.worldName;
    }

    @Override
    public WorldPosition add(Position position) {
        return new WorldPosition(this.worldName, this.x + position.getX(), this.y + position.getY(), this.z + position.getZ(), this.yaw, this.pitch);
    }

    @Override
    public WorldPosition add(Vector vector) {
        return new WorldPosition(this.worldName, this.x + vector.getX(), this.y + vector.getY(), this.z + vector.getZ(), this.yaw, this.pitch);
    }

    @Override
    public WorldPosition add(double x, double y, double z) {
        return new WorldPosition(this.worldName, this.x + x, this.y + y, this.z + z, this.yaw, this.pitch);
    }

    @Override
    public WorldPosition subtract(Position position) {
        return new WorldPosition(this.worldName, this.x - position.getX(), this.y - position.getY(), this.z - position.getZ(), this.yaw, this.pitch);
    }

    @Override
    public WorldPosition subtract(Vector vector) {
        return new WorldPosition(this.worldName, this.x - vector.getX(), this.y - vector.getY(), this.z - vector.getZ(), this.yaw, this.pitch);
    }

    @Override
    public WorldPosition subtract(double x, double y, double z) {
        return new WorldPosition(this.worldName, this.x - x, this.y - y, this.z - z, this.yaw, this.pitch);
    }

    public Location toLocation() {
        return new Location(this.getWorld(), this.x, this.y, this.z, this.yaw, this.pitch);
    }

    public boolean isChunkLoaded() {
        return this.getWorld().isChunkLoaded(this.getChunkX(), this.getChunkZ());
    }

    @Override
    public WorldPosition asBlockPos() {
        return new WorldPosition(this.worldName, this.getBlockX(), this.getBlockY(), this.getBlockZ(), this.yaw, this.pitch);
    }

    @Override
    public WorldPosition asCleanBlockPos() {
        return new WorldPosition(this.worldName, this.getBlockX(), this.getBlockY(), this.getBlockZ(), 0.0F, 0.0F);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        WorldPosition position = (WorldPosition) o;
        return this.worldName.equals(position.getWorldName()) &&
                Double.compare(position.x, x) == 0 &&
                Double.compare(position.y, y) == 0 &&
                Double.compare(position.z, z) == 0 &&
                Float.compare(position.yaw, yaw) == 0 &&
                Float.compare(position.pitch, pitch) == 0;
    }

    @Override
    public boolean equalsXYZ(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        WorldPosition position = (WorldPosition) o;
        return Double.compare(position.x, x) == 0 &&
                Double.compare(position.y, y) == 0 &&
                Double.compare(position.z, z) == 0;
    }

    @Override
    public String toString() {
        return this.worldName.toString() + ":" + super.toString();
    }
}

