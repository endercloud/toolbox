package com.rhonim.toolbox.toolbox;

import com.google.gson.JsonObject;
import com.google.inject.Singleton;
import com.rhonim.toolbox.Toolbox;
import com.rhonim.toolbox.database.Database;
import com.rhonim.toolbox.entities.Configurable;
import com.rhonim.toolbox.injection.GuiceServiceLoader;
import com.rhonim.toolbox.util.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

@Singleton
public final class ToolboxServices {

    private boolean booted = false;

    public void boot() {
        if (this.booted) {
            return;
        }
        this.booted = true;

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(ToolboxServices.class.getClassLoader());

        File configFile = new File(Toolbox.getInstance().getDataFolder(), "toolbox.json");
        if (!configFile.exists()) {
            try (FileOutputStream fos = new FileOutputStream(configFile)) {
                JsonObject object = new JsonObject();
                GuiceServiceLoader.load(Configurable.class, getClass().getClassLoader()).forEach(configurable -> {
                    configurable.setup(object);
                });
                fos.write(Gson.GSON.toJson(object).getBytes());
                fos.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try (FileReader reader = new FileReader(configFile)) {
                JsonObject config = Gson.GSON.fromJson(reader, JsonObject.class);

                GuiceServiceLoader.load(Configurable.class, getClass().getClassLoader()).forEach(configurable -> {
                    configurable.configure(Gson.GSON.fromJson(config.get(configurable.getConfigKey()), configurable.getConfigType()));
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        GuiceServiceLoader.load(Database.class, getClass().getClassLoader()).forEach(Database::connect);
        Thread.currentThread().setContextClassLoader(loader);

    }

}
