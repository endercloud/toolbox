package com.rhonim.toolbox.repo;

import com.google.inject.Singleton;
import com.rhonim.toolbox.entities.customitems.CustomItem;

import java.util.HashMap;
import java.util.Map;

@Singleton
public class CustomItemRepository {
    private final Map<String, CustomItem<?>> registeredItems = new HashMap<>();
    private final Map<String, CustomItem<?>> itemNameIndex = new HashMap<>();

    public <D extends CustomItem.Data> void registerCustomItem(CustomItem<D> item) {
        this.registeredItems.put(item.getClass().getCanonicalName(), item);
        this.itemNameIndex.put(item.getCmdName(), item);
    }

    public <D extends CustomItem.Data> CustomItem<D> getCustomItemFromType(String type) {
        return (CustomItem<D>) this.registeredItems.get(type);
    }

    public <D extends CustomItem.Data> CustomItem<D> getCustomItemFromName(String name) {
        return (CustomItem<D>) this.itemNameIndex.get(name);
    }


}
