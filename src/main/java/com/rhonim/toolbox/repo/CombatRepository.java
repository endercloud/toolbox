package com.rhonim.toolbox.repo;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Singleton;
import com.rhonim.toolbox.entities.CombatTag;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
public class CombatRepository {

    private final Map<UUID, Map<UUID, CombatTag>> attackerCache = new ConcurrentHashMap<>();
    private final Map<UUID, Map<UUID, CombatTag>> victimCache = new ConcurrentHashMap<>();

    public void registerCombatTag(CombatTag tag) {
        Map<UUID, CombatTag> attackerCombatTags;
        if ((attackerCombatTags = this.attackerCache.get(tag.getAttacker())) == null) {
            attackerCombatTags = new HashMap<>();
            this.attackerCache.put(tag.getAttacker(), attackerCombatTags);
        }
        attackerCombatTags.put(tag.getVictim(), tag);

        Map<UUID, CombatTag> victimCombatTags;
        if ((victimCombatTags = this.victimCache.get(tag.getVictim())) == null) {
            victimCombatTags = new HashMap<>();
            this.victimCache.put(tag.getVictim(), victimCombatTags);
        }

        victimCombatTags.put(tag.getAttacker(), tag);
    }

    public void unregisterTag(CombatTag tag) {
        Map<UUID, CombatTag> map = this.attackerCache.get(tag.getAttacker());
        if (map != null) {
            map.remove(tag.getVictim());
        }

        map = this.victimCache.get(tag.getVictim());
        if (map != null) {
            map.remove(tag.getAttacker());
        }
    }

    public void unTag(Player player) {
        this.unTag(player.getUniqueId());
    }

    public void unTag(UUID uuid) {
        for (CombatTag tag : this.getTags(uuid).values()) {
            this.unregisterTag(tag);
        }
    }

    public ImmutableMap<UUID, CombatTag> getTags(Player player) {
        return this.getTags(player.getUniqueId());
    }

    public ImmutableMap<UUID, CombatTag> getTags(UUID uuid) {
        Map<UUID, CombatTag> map;
        if ((map = this.attackerCache.get(uuid)) != null) {
            return ImmutableMap.copyOf(map);
        }

        if ((map = this.victimCache.get(uuid)) != null) {
            return ImmutableMap.copyOf(map);
        }

        return ImmutableMap.of();
    }

    public List<CombatTag> getActiveTags(Player player) {
        List<CombatTag> tags = new ArrayList<>();
        for (CombatTag tag : this.getTags(player).values()) {
            if (tag.isActive()) {
                tags.add(tag);
            }
        }

        return tags;
    }

    public void cleanMaps() {
        this.cleanUpMap(this.attackerCache);
        this.cleanUpMap(this.victimCache);
    }

    private void cleanUpMap(Map<UUID, Map<UUID, CombatTag>> overallMap) {
        Iterator<Map.Entry<UUID, Map<UUID, CombatTag>>> overallMapIt = overallMap.entrySet().iterator();
        while (overallMapIt.hasNext()) {
            Map<UUID, CombatTag> map = overallMapIt.next().getValue();
            if (map.isEmpty()) {
                overallMapIt.remove();
                continue;
            }

            Iterator<Map.Entry<UUID, CombatTag>> innerMapIt = map.entrySet().iterator();
            while (innerMapIt.hasNext()) {
                CombatTag tag = innerMapIt.next().getValue();
                if (!tag.isActive()) {
                    innerMapIt.remove();
                }
            }
        }

    }
}