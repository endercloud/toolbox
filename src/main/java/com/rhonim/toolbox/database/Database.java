package com.rhonim.toolbox.database;

import com.rhonim.toolbox.entities.Configurable;

public interface Database<C> extends Configurable<C> {

    public void connect();

}
