package com.rhonim.toolbox.database;

public class DatabaseConfig {

    private String host;
    private int port;
    private String username;
    private String password;
    private String database;

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDatabase() {
        return database;
    }
}
