package com.rhonim.toolbox.database.mysql;

import com.google.auto.service.AutoService;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.rhonim.toolbox.database.Database;
import com.rhonim.toolbox.database.DatabaseConfig;
import com.rhonim.toolbox.entities.Configurable;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.di.ClassLoaderManager;
import org.apache.cayenne.log.JdbcEventLogger;
import org.apache.cayenne.log.NoopJdbcEventLogger;

@Singleton
@AutoService({Configurable.class, Database.class})
public class MySQL implements Database<DatabaseConfig> {

    private MysqlDataSource dataSource;

    private ServerRuntime runtime;

    @Inject(optional = true)
    private PluginCayanneClassLoaderProvider pluginCayanneClassLoaderProvider;

    @Override
    public void connect() {
        ClassLoader loader;
        if (this.pluginCayanneClassLoaderProvider != null) {
            loader = this.pluginCayanneClassLoaderProvider.getClass().getClassLoader();
        } else {
            loader = getClass().getClassLoader();
        }
        System.out.println("Providing " + loader + " to cayenne to use.");

        if (loader.getResource("cayenne-database.xml") == null) {
            System.err.println("Skipping Cayenne boot due to no datamap!");
            return;
        }

        ClassLoader contextLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(MySQL.class.getClassLoader());

        this.runtime = ServerRuntime.builder()
                .jdbcDriver("com.mysql.jdbc.jdbc2.optional.MysqlDataSource")
                .minConnections(5)
                .maxConnections(50)
                .dataSource(this.dataSource)
                .addConfig("cayenne-database.xml")
                .addModule(binder -> {
                    binder.bind(JdbcEventLogger.class).to(NoopJdbcEventLogger.class);
                    binder.bind(ClassLoaderManager.class).toInstance(new PluginClassLoaderManager(loader));
//                    binder.bind(AdhocObjectFactory.class).to(PluginAdhocObjectFactory.class);
                })
                .build();

        Thread.currentThread().setContextClassLoader(contextLoader);
    }

    @Override
    public String getConfigKey() {
        return "mysql";
    }

    @Override
    public Class<DatabaseConfig> getConfigType() {
        return DatabaseConfig.class;
    }

    @Override
    public void configure(DatabaseConfig config) {
        if (!isCayenneLoaded()) {
            return;
        }

        MysqlDataSource mysql = new MysqlDataSource();
        mysql.setServerName(config.getHost());
        mysql.setPort(config.getPort());
        mysql.setDatabaseName(config.getDatabase());
        mysql.setUser(config.getUsername());
        mysql.setPassword(config.getPassword());
        mysql.setAutoReconnect(true);
        mysql.setUseSSL(false);
        mysql.setTcpKeepAlive(true);

        this.dataSource = mysql;
    }

    @Override
    public void setup(JsonObject object) {
        JsonObject obj = new JsonObject();
        obj.addProperty("host", "127.0.0.1");
        obj.addProperty("port", 3306);
        obj.addProperty("username", "root");
        obj.addProperty("password", "");
        obj.addProperty("database", "minecraft");
        object.add("mysql", obj);
    }

    private boolean isCayenneLoaded() {
        try {
            Class.forName("org.apache.cayenne.ObjectContext");
        } catch (ClassNotFoundException e) {
            return false;
        }
        return true;
    }


    public ObjectContext getNewContext() {
        return this.runtime.newContext();
    }

    public ServerRuntime getRuntime() {
        return runtime;
    }
}
