package com.rhonim.toolbox.database.mysql;

import org.apache.cayenne.di.ClassLoaderManager;

public class PluginClassLoaderManager implements ClassLoaderManager {

    private ClassLoader loader;

    public PluginClassLoaderManager(ClassLoader loader) {
        this.loader = loader;
    }

    @Override
    public ClassLoader getClassLoader(String resourceName) {
        if(resourceName.equalsIgnoreCase("datamap.map.xml") || resourceName.equalsIgnoreCase("cayenne-database.xml")) {
            return loader;
        } else {
            return PluginClassLoaderManager.class.getClassLoader();
        }
    }
}
