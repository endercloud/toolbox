package com.rhonim.toolbox.database;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;


public class AsyncDatabase {

    private static final AtomicInteger threadCount = new AtomicInteger(0);
    private static final ExecutorService executorService = Executors.newCachedThreadPool(r -> {
        Thread thread = new Thread(r, "Database Thread - " + threadCount.incrementAndGet());
        thread.setDaemon(true);
        return thread;
    });

    AsyncDatabase() {
    }

    public static void submit(Runnable runnable) {
        executorService.submit(() -> {
            try {
                runnable.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
